package lab_4.DAO;

import lab_4.Parser;

import java.sql.*;
import java.util.Objects;
import java.util.Scanner;

public class JDBCDataAccessObject implements DataAccessObject {
    private String database = "assortiment";
    private String table = "items";
    private String user = "root";
    private String password = "orangejuice";
    private Connection connection;
    private PreparedStatement statement;
    private String url;

    static private JDBCDataAccessObject DAO;

    private JDBCDataAccessObject() {
        url = "jdbc:mysql://localhost:3306/" + database + "?useUnicode=true&serverTimezone=UTC";
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException sqlEx) {
            System.out.println("can't connect");
            sqlEx.printStackTrace();
        }
    }

    public static JDBCDataAccessObject getJDBCDataAccessObject() {
        return Objects.requireNonNullElseGet(DAO, JDBCDataAccessObject::new);
    }

    @Override
    public void disconnect(Scanner scanner) {
        try {
            connection.close();
        } catch (SQLException se) {
            System.out.println();
        }
    }

    @Override
    public void clean(Scanner scanner) {
        String query = "truncate table " + table;
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);

            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void changeCost(Scanner scanner) {
        String title;
        int cost;
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (scanner.hasNextInt()) {
            cost = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }

        String query = "update " + table + " set cost = ? where title = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, cost);
            statement.setString(2, title);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void deleteById(Scanner scanner) {
        String id;

        if (scanner.hasNext()) {
            id = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }

        String query = "delete from " + table + " where id = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void deleteByProdid(Scanner scanner) {
        int prodid;

        if (scanner.hasNextInt()) {
            prodid = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }

        String query = "delete from " + table + " where prodid = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, prodid);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {

            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void deleteByTitle(Scanner scanner) {
        String title;

        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }

        String query = "delete from " + table + " where title = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, title);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void insert(Scanner scanner) {
        String id;
        int prodid;
        String title;
        int cost;
        if (scanner.hasNext()) {
            id = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (scanner.hasNextInt()) {
            prodid = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (scanner.hasNextInt()) {
            cost = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }

        String query = "insert into assortiment.items(id, prodid, title, cost) values (?, ?, ?, ?)";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);

            statement.setString(1, id);
            statement.setInt(2, prodid);
            statement.setString(3, title);
            statement.setInt(4, cost);

            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void printByCost(Scanner scanner) {
        String title;
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        int cost = 0;
        String query = "select cost from " + table + " where title = ?";

        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, title);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getInt("cost"));
            } else {
                System.out.println("item with title " + title + " not found");
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void printByCostRange(Scanner scanner) {
        int min;
        int max;
        if (scanner.hasNextInt()) {
            min = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (scanner.hasNextInt()) {
            max = scanner.nextInt();
        } else {
            System.out.println("invalid command arguments");
            return;
        }
        if (min > max) {
            System.out.println("min must be < max");
            return;
        }
        String query = "select id, prodid, title, cost from assortiment.items where cost > ? and cost < ? ";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, min);
            statement.setInt(2, max);
            ResultSet resultSet = statement.executeQuery();

            boolean empty = true;
            while (resultSet.next()) {
                empty = false;
                String id = resultSet.getString("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                System.out.println(id + " " + prodid + " " + title + " " + cost);
            }

            if (empty) {
                System.out.println("no items found with suitable cost");
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    @Override
    public void showAll(Scanner scanner) {
        String query = "select * from " + table;
        try {
            statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                System.out.println(id + " " + prodid + " " + title + " " + cost);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }
}
