package lab_4.DAO;

import java.sql.*;
import java.util.Scanner;

public interface DataAccessObject {

    public abstract void clean(Scanner scanner);

    public abstract void changeCost(Scanner scanner);

    public abstract void deleteById(Scanner scanner);

    public abstract void deleteByProdid(Scanner scanner);

    public abstract void deleteByTitle(Scanner scanner);

    public abstract void insert(Scanner scanner);

    public abstract void printByCost(Scanner scanner);

    public abstract void printByCostRange(Scanner scanner);

    public abstract void showAll(Scanner scanner);

    public abstract void disconnect(Scanner scanner);
}
