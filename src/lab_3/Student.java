package lab_3;

public class Student implements Comparable<Student> {
    private int labs;
    private Subject subject;

    public Student(int labs, Subject subject) {
        this.labs = labs;
        this.subject = subject;
    }

    public int getLabs() {
        return labs;
    }

    public Subject getSubject() {
        return subject;
    }

    public static Student createRandomStudent() {
        Subject subject;

        int rand = (int) (Math.random() * 3);
        switch (rand) {
            case 0:
                subject = Subject.MATHS;
                break;
            case 1:
                subject = Subject.PHYSICS;
                break;
            case 2:
                subject = Subject.PROGRAMMING;
                break;
            default:
                throw new IndexOutOfBoundsException("illegal random number");
        }
        rand = (int) (Math.random() * 3);
        int labs;
        switch (rand) {
            case 0:
                labs = 10;
                break;
            case 1:
                labs = 20;
                break;
            case 2:
                labs = 100;
                break;
            default:
                throw new IndexOutOfBoundsException("illegal random number");
        }

        return new Student(labs, subject);
    }

    @Override
    public int compareTo(Student student) {
        return Integer.compare(this.labs, student.labs);
    }
}
