package lab_3;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.lang.Thread.sleep;

public class TripleQueue {
    private Map<Subject, Queue<Student>> quees = new HashMap(3);
    private Map<Subject, SimpleRobot> consumers = new HashMap(3);
    private RandomStudentGenerator generator;
    final private int consumersCount = 3;
    public final int totalCapacity = 10;
    private boolean generate = true;

    public TripleQueue() throws InterruptedException {
        quees.put(Subject.MATHS, new ConcurrentLinkedQueue<Student>());
        quees.put(Subject.PHYSICS, new ConcurrentLinkedQueue<Student>());
        quees.put(Subject.PROGRAMMING, new ConcurrentLinkedQueue<Student>());

        SimpleRobot programmer = new SimpleRobot("киборг Мартынов", Subject.PROGRAMMING, this);
        SimpleRobot physicist = new SimpleRobot("киборг Попов", Subject.PHYSICS, this);
        SimpleRobot mathematician = new SimpleRobot("Подсыпанин", Subject.MATHS, this);

        generator = new RandomStudentGenerator(this);

        this.setConsumers(mathematician, physicist, programmer);

        generator.start();

        sleep(2000);
        physicist.start();
        programmer.start();
        mathematician.start();
    }

    public void add(Student newStudent, boolean notify) throws InterruptedException {
        quees.get(newStudent.getSubject()).add(newStudent);
        if (notify) {
            synchronized (consumers.get(newStudent.getSubject())) {
                consumers.get(newStudent.getSubject()).notify();
            }
        }
        System.out.println("    к преподу по " + newStudent.getSubject() + " поступил студент");
        System.out.println("    количество лаб: " + newStudent.getLabs());
        System.out.println("    размер очереди: " + quees.get(newStudent.getSubject()).size());
        System.out.println("    размер общей очереди: " + getSize());

        if (getSize() >= totalCapacity) {
            synchronized (generator) {
                generator.wait();
            }
        }
    }

    public Student poll(Subject subject) throws InterruptedException {
        Student student;
        synchronized (consumers.get(subject)) {
            if (quees.get(subject).size() > 0) {
                student = quees.get(subject).poll();
            } else {
                if (!generate) {
                    consumers.get(subject).close();
                } else {
                    System.out.println("Препод \"" + consumers.get(subject).getName() + "\" пьет чай");
                    consumers.get(subject).wait();
                }
                student = null;
            }

            synchronized (generator) {
                if (generate) {
                    generator.notify();
                }
            }

            return student;
        }
    }

    private void notifyGenerator() {
        synchronized (generator) {
            generator.notify();
        }
    }

    public Queue getMap(Subject subject) {
        return quees.get(subject);
    }

    public void setConsumers(SimpleRobot mathematic, SimpleRobot physicist, SimpleRobot progranner) {
        consumers.put(Subject.MATHS, mathematic);
        consumers.put(Subject.PROGRAMMING, progranner);
        consumers.put(Subject.PHYSICS, physicist);
    }

    public int getSize() {
        return (quees.get(Subject.MATHS).size() + quees.get(Subject.PHYSICS).size() + quees.get(Subject.PROGRAMMING).size());
    }

    public void stopGenerate() {
        System.out.println("Вход в корпус закрылся");
        generate = false;
        synchronized (generator) {
            generator.close();
            generator.notify();
        }
        synchronized (consumers.get(Subject.MATHS)) {
            consumers.get(Subject.MATHS).notify();
        }
        synchronized (consumers.get(Subject.PHYSICS)) {
            consumers.get(Subject.PHYSICS).notify();
        }
        synchronized (consumers.get(Subject.PROGRAMMING)) {
            consumers.get(Subject.PROGRAMMING).notify();
        }
    }
}
