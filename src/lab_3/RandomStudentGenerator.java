package lab_3;

public class RandomStudentGenerator extends Thread implements StudentGenerator {

    private TripleQueue queue;
    private boolean close = false;

    public RandomStudentGenerator(TripleQueue queue) {
        this.queue = queue;
        System.out.println("--- успешно создан генератор студентов ---");
    }

    @Override
    public void run() {
        try {
            System.out.println("--- запущен генератор студентов ---");
            do {
                Student newStudent = Student.createRandomStudent();
                queue.add(newStudent, true);
            }
            while (!close);
            System.out.println("--- остановлен генератор студентов ---");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void close() {
        close = true;
    }
}
