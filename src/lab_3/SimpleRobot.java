package lab_3;

import java.util.Queue;

public class SimpleRobot extends Thread implements Robot {

    final protected int cabinetCapacity = 10;
    final protected int labsPerSecond = 5;

    private Subject subject;
    private boolean close = false;
    private TripleQueue queue;

    public SimpleRobot(String name, Subject subject, TripleQueue queue) throws InterruptedException {
        super();
        this.setName(name);
        this.queue = queue;
        this.subject = subject;
        System.out.println("Успешно создан препод " + name + " по предмету " + subject);
    }

    @Override
    public void run() {
        try {
            System.out.println("Препод \"" + getName() + "\" приступил к работе");
            do {
                Student newStudent = null;
                try {
                    while ((newStudent == null) && !close) {
                        newStudent = queue.poll(subject);
                    }
                    if (close) break;
                    sleep(newStudent.getLabs() / labsPerSecond * 1000);
                    System.out.println("Студент обработан преподом \"" + getName() + "\" за " + newStudent.getLabs() / labsPerSecond + " секунд(ы)");
                } catch (NullPointerException e) {
                    System.out.println("Препод \"" + getName() + "\" увидел привидение");
                }
            }
            while (!close);
            System.out.println("Препод \"" + getName() + "\" собирает манатки");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void close() {
        close = true;
    }

}
