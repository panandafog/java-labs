package lab_1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Both interval borders should be > 0");
        System.out.println("Input minimum border: ");
        if (!input.hasNextDouble()) {
            System.out.println("Wrong input ");
            return;
        }

        double min = input.nextDouble();


//        if (input.hasNext()) {
//            System.out.println("Wrong input ");
//            return;
//        }

        System.out.println("Input maximum border: ");
        if (!input.hasNextDouble()) {
            System.out.println("Wrong input ");
            return;
        }

//        if (input.hasNext()) {
//            System.out.println("Wrong input ");
//            return;
//        }

        double max = input.nextDouble();

        if (max < min) {
            System.out.println("Maximum border is smaller then minimum");
            return;
        }

        if (max <= 0 || min <= 0) {
            System.out.println("Both interval borders should be > 0");
            return;
        }

        min = Math.ceil(min);
        max = Math.floor(max);

        for (long i = (long) min; i <= (long) max; i++) {
            boolean isSimple;
            isSimple = i > 1;
            for (long j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    isSimple = false;
                    break;
                }
            }
            if (isSimple) {
                System.out.print(i + "  ");
            }
        }

        //System.out.printf("Your interval: %f :: %f \n", min, max);
        //System.out.println("Your interval: " + min + " :: " + max);
        input.close();
    }
}

