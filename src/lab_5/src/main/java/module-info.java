module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires mysql.connector.java;
    requires java.sql;

    opens lab_5 to javafx.fxml;
    exports lab_5;
}