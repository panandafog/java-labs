package lab_5;

import com.mysql.cj.exceptions.WrongArgumentException;
import lab_5.DAO.DataAccessObject;
import lab_5.DAO.JDBCDataAccessObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Parser {

    public Parser() {
    }

    public void parse() {
        DataAccessObject DAO = JDBCDataAccessObject.getJDBCDataAccessObject();
        Map<String, Method> executors = new HashMap<String, Method>();
        try {
            executors.put("clean", DAO.getClass().getDeclaredMethod("clean", Scanner.class));
            executors.put("change_price", DAO.getClass().getDeclaredMethod("changeCost", Scanner.class));
            executors.put("delete_id", DAO.getClass().getDeclaredMethod("deleteById", Scanner.class));
            executors.put("delete_prodid", DAO.getClass().getDeclaredMethod("deleteByProdid", Scanner.class));
            executors.put("delete_title", DAO.getClass().getDeclaredMethod("deleteByTitle", Scanner.class));
            executors.put("add", DAO.getClass().getDeclaredMethod("insert", Scanner.class));
            executors.put("price", DAO.getClass().getDeclaredMethod("printByCost", Scanner.class));
            executors.put("filter_by_price", DAO.getClass().getDeclaredMethod("printByCostRange", Scanner.class));
            executors.put("show_all", DAO.getClass().getDeclaredMethod("showAll", Scanner.class));
            executors.put("exit", DAO.getClass().getDeclaredMethod("disconnect", Scanner.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner("");
        DAO.clean(scanner);

        System.out.println("enter number of random initial entires:");
        scanner = new Scanner(System.in);
        int n = 0;
        if (scanner.hasNextInt()) {
            n = scanner.nextInt();
        }

        for (Integer index = 0; index < n; index++) {
            String string = new String(index + " " + index.toString() + " " + index);
            scanner = new Scanner(string);
            DAO.insert(scanner);
        }
        System.out.println("database initialised");
        System.out.println();
        System.out.println("available commands:");
        for (String key : executors.keySet()) {
            System.out.println(key);
        }
        System.out.println();
        System.out.println("command must start with /");
        System.out.println();
        System.out.println("write your command");
        System.out.println();

        Scanner lineScanner = new Scanner(System.in);
        while (lineScanner.hasNextLine()) {
            String line = lineScanner.nextLine();
            if (line.charAt(0) != '/') {
                System.out.println("command must start with /");
            } else {
                line = line.substring(1);
                Scanner wordScanner = new Scanner(line);
                if (!wordScanner.hasNext()) {
                    System.out.println("unknown command");
                } else {
                    String command = wordScanner.next();
                    if (!executors.containsKey(command)) {
                        System.out.println("unknown command");
                    } else {
                        try {
                            executors.get(command).invoke(DAO, wordScanner);
                        } catch (WrongArgumentException e) {
                            System.out.println("wrong input");
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        if (command.equals("exit")) {
                            System.out.println("bye");
                            return;
                        }
                    }

                }
            }
        }
    }
}
