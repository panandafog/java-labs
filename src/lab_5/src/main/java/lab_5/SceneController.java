package lab_5;

import com.mysql.cj.exceptions.WrongArgumentException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.WindowEvent;
import lab_5.DAO.DataAccessObject;
import lab_5.DAO.JDBCDataAccessObject;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class SceneController implements Initializable {

    @FXML
    private Button truncateButton;
    @FXML
    private Button insertButton;
    @FXML
    private Button changeCostButton;
    @FXML
    private Button deleteByIdButton;
    @FXML
    private Button deleteByProdidButton;
    @FXML
    private Button deleteByTitleButton;
    @FXML
    private Button searchCostButton;
    @FXML
    private Button searchByCostRangeButton;

    @FXML
    private TextField insertProdIDText;
    @FXML
    private TextField insertTitleText;
    @FXML
    private TextField insertCostText;
    @FXML
    private TextField changeCostTitle;
    @FXML
    private TextField changeCostCost;
    @FXML
    private TextField deleteByIDText;
    @FXML
    private TextField deleteByTitleText;
    @FXML
    private TextField deleteByProdidText;
    @FXML
    private TextField searchCostTitleText;
    @FXML
    private TextField searchByCostRangeMinCostText;
    @FXML
    private TextField searchByCostRangeMaxCostText;

    @FXML
    private TableView<Product> outputTableView = new TableView<Product>();

    private TableColumn<Product, Integer> idColumn = new TableColumn<Product, Integer>("id");
    private TableColumn<Product, Integer> prodidColumn = new TableColumn<Product, Integer>("prodid");
    private TableColumn<Product, String> titleColumn = new TableColumn<Product, String>("title");
    private TableColumn<Product, Integer> costColumn = new TableColumn<Product, Integer>("cost");
    @FXML
    private Button showAllButton;

    DataAccessObject DAO = JDBCDataAccessObject.getJDBCDataAccessObject();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("id"));
        prodidColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("prodid"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("title"));
        costColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("cost"));
        outputTableView.getColumns().addAll(idColumn, prodidColumn, titleColumn, costColumn);
    }

    private javafx.event.EventHandler<WindowEvent> closeEventHandler = event -> disconnect();

    public javafx.event.EventHandler<WindowEvent> getCloseEventHandler() {
        return closeEventHandler;
    }

    public void disconnect() {
        System.out.println("disconnecting");

        Scanner scanner = new Scanner("");
        DAO.disconnect(scanner);
    }

    public void clean(MouseEvent keyEvent) {
        Scanner scanner = new Scanner("");
        DAO.clean(scanner);
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void insert(MouseEvent keyEvent) {
        String input = insertProdIDText.getText() + " " + insertTitleText.getText() + " " + insertCostText.getText();

        try {
            DAO.insert(new Scanner(input));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void changeCost(MouseEvent keyEvent) {
        String input = changeCostTitle.getText() + " " + changeCostCost.getText();

        try {
            DAO.changeCost(new Scanner(input));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void deleteById(MouseEvent keyEvent) {
        String input = deleteByIDText.getText();
        try {
            DAO.deleteById(new Scanner(input));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void deleteByProdid(MouseEvent keyEvent) {
        String input = deleteByProdidText.getText();
        try {
            DAO.deleteByProdid(new Scanner(input));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void deleteByTitle(MouseEvent keyEvent) {
        String input = deleteByTitleText.getText();
        try {
            DAO.deleteByTitle(new Scanner(input));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void searchByCost(MouseEvent keyEvent) {
        String input = searchCostTitleText.getText();

        ObservableList<Product> result = null;
        try {
            result = FXCollections.observableArrayList(DAO.getByCost(new Scanner(input)));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(result);
    }

    public void searchByCostRange(MouseEvent keyEvent) {

        String input = searchByCostRangeMinCostText.getText() + " " + searchByCostRangeMaxCostText.getText();
        try {
            outputTableView.setItems(DAO.getByCostRange(new Scanner(input)));
        } catch (WrongArgumentException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Wrong input", ButtonType.CLOSE);
            alert.showAndWait();
        }
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }

    public void showAll(MouseEvent mouseEvent) {
        outputTableView.setItems(DAO.getAll(new Scanner("")));
    }
}