package lab_5;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Product {

    private SimpleIntegerProperty id;
    private SimpleIntegerProperty prodid;
    private SimpleStringProperty title;
    private SimpleIntegerProperty cost;

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getProdid() {
        return prodid.get();
    }

    public SimpleIntegerProperty prodidProperty() {
        return prodid;
    }

    public void setProdid(int prodid) {
        this.prodid.set(prodid);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public int getCost() {
        return cost.get();
    }

    public SimpleIntegerProperty costProperty() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost.set(cost);
    }

    public Product(int id, int prodid, String title, int cost) {
        this.id = new SimpleIntegerProperty(id);
        this.prodid = new SimpleIntegerProperty(prodid);
        this.title = new SimpleStringProperty(title);
        this.cost = new SimpleIntegerProperty(cost);
    }

}
