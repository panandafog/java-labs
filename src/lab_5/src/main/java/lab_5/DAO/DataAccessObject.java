package lab_5.DAO;

import javafx.collections.ObservableList;
import lab_5.Product;

import java.util.Scanner;

public interface DataAccessObject {

    public void clean(Scanner scanner);

    public void changeCost(Scanner scanner);

    public void deleteById(Scanner scanner);

    public void deleteByProdid(Scanner scanner);

    public void deleteByTitle(Scanner scanner);

    public void insert(Scanner scanner);

    public void printByCost(Scanner scanner);

    public void printByCostRange(Scanner scanner);

    public void showAll(Scanner scanner);

    public void disconnect(Scanner scanner);

    public ObservableList<Product> getByCostRange(Scanner scanner);

    public Product getByCost(Scanner scanner);

    public ObservableList<Product> getAll(Scanner scanner);
}
