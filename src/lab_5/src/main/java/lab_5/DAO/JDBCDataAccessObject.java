package lab_5.DAO;

import com.mysql.cj.exceptions.WrongArgumentException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lab_5.Product;

import java.sql.*;
import java.util.Objects;
import java.util.Scanner;

public class JDBCDataAccessObject implements DataAccessObject {
    private String database = "assortiment";
    private String table = "items";
    private String user = "root";
    private String password = "orangejuice";
    private Connection connection;
    private PreparedStatement statement;
    private String url;

    static private JDBCDataAccessObject DAO;

    private JDBCDataAccessObject() {
        url = "jdbc:mysql://localhost:3306/" + database + "?useUnicode=true&serverTimezone=UTC";
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException sqlEx) {
            System.out.println("can't connect");
            sqlEx.printStackTrace();
        }
    }

    public static JDBCDataAccessObject getJDBCDataAccessObject() {
        return Objects.requireNonNullElseGet(DAO, JDBCDataAccessObject::new);
    }

    public void disconnect(Scanner scanner) {
        try {
            connection.close();
        } catch (SQLException se) {
            System.out.println();
        }
    }

    public void clean(Scanner scanner) {
        String query = "truncate table " + table;
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);

            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void changeCost(Scanner scanner) throws WrongArgumentException {
        String title;
        int cost;
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }
        if (scanner.hasNextInt()) {
            cost = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }

        String query = "update " + table + " set cost = ? where title = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, cost);
            statement.setString(2, title);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void deleteById(Scanner scanner) throws WrongArgumentException {
        String id;

        if (scanner.hasNext()) {
            id = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }

        String query = "delete from " + table + " where id = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void deleteByProdid(Scanner scanner) throws WrongArgumentException {
        int prodid;

        if (scanner.hasNextInt()) {
            prodid = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }

        String query = "delete from " + table + " where prodid = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, prodid);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {

            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void deleteByTitle(Scanner scanner) throws WrongArgumentException {
        String title;

        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }

        String query = "delete from " + table + " where title = ?";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, title);
            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void insert(Scanner scanner) throws WrongArgumentException {
        int prodid;
        String title;
        int cost;
        if (scanner.hasNextInt()) {
            prodid = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }
        if (scanner.hasNextInt()) {
            cost = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }

        String query = "insert into assortiment.items(prodid, title, cost) values (?, ?, ?)";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);

            statement.setInt(1, prodid);
            statement.setString(2, title);
            statement.setInt(3, cost);

            statement.execute();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void printByCost(Scanner scanner) throws WrongArgumentException {
        String title;
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }
        String query = "select cost from " + table + " where title = ?";

        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, title);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getInt("cost"));
            } else {
                System.out.println("item with title " + title + " not found");
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void printByCostRange(Scanner scanner) throws WrongArgumentException {
        int min;
        int max;
        if (scanner.hasNextInt()) {
            min = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }
        if (scanner.hasNextInt()) {
            max = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }
        if (min > max) {
            throw (new WrongArgumentException());
        }
        String query = "select id, prodid, title, cost from assortiment.items where cost > ? and cost < ? ";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, min);
            statement.setInt(2, max);
            ResultSet resultSet = statement.executeQuery();

            boolean empty = true;
            while (resultSet.next()) {
                empty = false;
                String id = resultSet.getString("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                System.out.println(id + " " + prodid + " " + title + " " + cost);
            }

            if (empty) {
                System.out.println("no items found with suitable cost");
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void showAll(Scanner scanner) {
        String query = "select * from " + table;
        try {
            statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                System.out.println(id + " " + prodid + " " + title + " " + cost);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public Product getByCost(Scanner scanner) throws WrongArgumentException {
        String title = "";
        if (scanner.hasNext()) {
            title = scanner.next();
        } else {
            throw (new WrongArgumentException());
        }
        String query = "select * from " + table + " where title = ?";

        Product result = null;
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setString(1, title);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                int prodid = resultSet.getInt("prodid");
                String resTitle = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                result = new Product(id, prodid, resTitle, cost);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return result;
    }

    public ObservableList<Product> getByCostRange(Scanner scanner) throws WrongArgumentException {
        ObservableList<Product> result =
                FXCollections.observableArrayList();
        int min = 0;
        int max = 0;
        if (scanner.hasNextInt()) {
            min = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }
        if (scanner.hasNextInt()) {
            max = scanner.nextInt();
        } else {
            throw (new WrongArgumentException());
        }
        if (min > max) {
            throw (new WrongArgumentException());
        }
        String query = "select id, prodid, title, cost from assortiment.items where cost >= ? and cost <= ? ";
        try {
            // getting Statement object to execute query
            statement = connection.prepareStatement(query);
            statement.setInt(1, min);
            statement.setInt(2, max);
            ResultSet resultSet = statement.executeQuery();

            boolean empty = true;
            while (resultSet.next()) {
                empty = false;
                int id = resultSet.getInt("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                result.add(new Product(id, prodid, title, cost));
            }

            if (empty) {
                System.out.println("no items found with suitable cost");
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return result;
    }

    public ObservableList<Product> getAll(Scanner scanner) {
        ObservableList<Product> result =
                FXCollections.observableArrayList();
        String query = "select * from " + table;
        try {
            statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int prodid = resultSet.getInt("prodid");
                String title = resultSet.getString("title");
                int cost = resultSet.getInt("cost");
                result.add(new Product(id, prodid, title, cost));
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
        return result;
    }
}
