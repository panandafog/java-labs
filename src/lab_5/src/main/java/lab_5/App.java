package lab_5;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root;
        URL url = new File("src/main/resources/scene.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);
        try {
            root = (Parent) loader.load();
        } catch (Exception e) {
            System.out.println("Loading fxml failed...");
            System.out.println(e);
            return;
        }
        SceneController controller = loader.getController();
        Scene scene = new Scene(root);
        primaryStage.setTitle("Database client");
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(controller.getCloseEventHandler());
    }

    public static void main(String[] args) { launch(); }

}