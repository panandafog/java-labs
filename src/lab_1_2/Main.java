package lab_1_2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Both interval borders should be > 0");
        System.out.println("Input minimum border: ");
        if (!input.hasNextBigDecimal()) {
            System.out.println("Wrong input ");
            return;
        }

        BigDecimal min = input.nextBigDecimal();


//        if (input.hasNext()) {
//            System.out.println("Wrong input ");
//            return;
//        }

        System.out.println("Input maximum border: ");
        if (!input.hasNextBigDecimal()) {
            System.out.println("Wrong input ");
            return;
        }

//        if (input.hasNext()) {
//            System.out.println("Wrong input ");
//            return;
//        }

        BigDecimal max = input.nextBigDecimal();

        if (max.compareTo(min) < 0) {
            System.out.println("Maximum border is smaller then minimum");
            return;
        }

        if (max.compareTo(BigDecimal.ZERO) <= 0 || min.compareTo(BigDecimal.ZERO) <= 0) {
            System.out.println("Both interval borders should be > 0");
            return;
        }

        min = min.setScale(0, RoundingMode.CEILING);
        max = max.setScale(0, RoundingMode.FLOOR);
        ;

        for (BigInteger i = min.toBigInteger(); i.compareTo(max.toBigInteger()) <= 0; i = i.add(BigInteger.ONE)) {
            boolean isSimple;
            isSimple = (i.compareTo(BigInteger.ONE) > 0); //i>1
            for (BigInteger j = BigInteger.TWO; j.compareTo(i.divide(BigInteger.TWO)) <= 0; j = j.add(BigInteger.ONE)) { //j <= i /
                if (i.mod(j).equals(BigInteger.ZERO)) {
                    isSimple = false;
                    break;
                }
            }
            if (isSimple) {
                System.out.print(i + "  ");
            }
        }

        //System.out.printf("Your interval: %f :: %f \n", min, max);
        //System.out.println("Your interval: " + min + " :: " + max);
        input.close();
    }
}
