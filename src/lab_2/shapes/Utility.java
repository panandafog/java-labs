package lab_2.shapes;

public class Utility {
    static double distanceBetweenPoints(Point first, Point second) {
        return (Math.sqrt(Math.pow(second.getX() - first.getX(), 2) + Math.pow(second.getY() - first.getY(), 2)));
    }
}
