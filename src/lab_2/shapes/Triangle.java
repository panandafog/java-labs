package lab_2.shapes;

import java.util.ArrayList;
import java.util.Iterator;

import static java.lang.Math.abs;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Triangle implements Polygon {

    private ArrayList<Point> points;
    private double rotation;

    public Triangle(SimplePoint a, SimplePoint b, SimplePoint c) throws IllegalArgumentException {

        double precision = 0.001;

        if (abs((c.getX() - a.getX()) * (b.getY() - a.getY()) - (c.getY() - a.getY()) * (b.getX() - a.getX()))
                < precision) {
            throw new IllegalArgumentException("vertexes of triangle can't lie on the same straight line or match");
        }

        points = new ArrayList<Point>();
        points.add(a);
        points.add(b);
        points.add(c);
    }

    public double getArea() {
        return Math.abs((points.get(0).getX() - points.get(2).getX()) * (points.get(1).getY() - points.get(2).getY())
                - (points.get(1).getX() - points.get(2).getX()) * (points.get(0).getY() - points.get(2).getY())) / 2;
    }

    protected Point getCentre() {
        Point centre = new SimplePoint(0, 0);

        Iterator<Point> it = points.iterator();

        if (it.hasNext()) do {
            centre.moveTo(it.next());

        } while (it.hasNext());

        return (centre);
    }

    public void rotate(double degree) {
        rotation += degree;
        double cos = cos((2 * Math.PI * degree) / 360);
        double sin = sin((2 * Math.PI * degree) / 360);

        Point centre = getCentre();

        for (int i = 0; i < points.size(); i++) {
            double x = centre.getX() + (points.get(i).getX() - centre.getX()) * cos - (points.get(i).getY() - centre.getY()) * sin;
            double y = centre.getY() + (points.get(i).getX() - centre.getX()) * sin + (points.get(i).getY() - centre.getY()) * cos;
            points.set(i, new SimplePoint(x, y));
        }
    }

    public double getPerimeter() {
        double perimeter = 0;
        Iterator<Point> it1 = points.iterator();
        Iterator<Point> it2 = points.iterator();

        if (it2.hasNext()) {
            it2.next();
        }

        if (it2.hasNext()) do {
            perimeter += Utility.distanceBetweenPoints(it1.next(), it2.next());
        } while (it2.hasNext());

        return perimeter;
    }

    public double getRotation() {
        return rotation;
    }

    public void moveTo(double x, double y) {
        Iterator<Point> it = points.iterator();

        if (it.hasNext()) do {
            it.next().moveTo(x, y);

        } while (it.hasNext());
    }

    public void moveTo(Point vector) {
        Iterator<Point> it = points.iterator();

        if (it.hasNext()) do {
            it.next().moveTo(vector);

        } while (it.hasNext());
    }

    public void moveIn(double x, double y) {
        Iterator<Point> it = points.iterator();

        if (it.hasNext()) do {
            it.next().moveIn(x, y);

        } while (it.hasNext());
    }

    public void moveIn(Point destination) {
        Iterator<Point> it = points.iterator();

        if (it.hasNext()) do {
            it.next().moveIn(destination);

        } while (it.hasNext());
    }
}
