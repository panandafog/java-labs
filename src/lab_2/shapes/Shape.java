package lab_2.shapes;

public interface Shape {

    double getArea();

    default double getRotation() {
        throw new UnsupportedOperationException("Class is not supporting method");
    }

    double getPerimeter();

    void rotate(double degree);

    void moveTo(double x, double y);

    void moveTo(Point vector);

    void moveIn(double x, double y);

    void moveIn(Point destination);

}
