package lab_2.shapes;

public interface Point {

    double getX();

    double getY();

    void moveTo(double x, double y);

    void moveTo(Point vector);

    void moveIn(double x, double y);

    void moveIn(Point destination);
}
