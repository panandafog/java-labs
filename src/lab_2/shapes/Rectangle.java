package lab_2.shapes;

import java.util.ArrayList;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

public class Rectangle implements Polygon {

    private double width, height;
    private SimplePoint centre;

    protected int rotation;

    public Rectangle(SimplePoint centre, double width, double height) {
        if (width <= 0) {
            throw new IllegalArgumentException("Width of rectangle must be positive");
        }
        if (height <= 0) {
            throw new IllegalArgumentException("Height of rectangle must be positive");
        }

        this.centre = centre;
        this.width = width;
        this.height = height;
        rotation = 0;
    }

    public double getArea() {
        return (width * height);
    }

    public double getPerimeter() {
        return ((width + height) * 2);
    }

    public void rotate(double degree) {
        rotation += degree;
    }

    public void moveTo(double x, double y) {
        centre.moveIn(x, y);
    }

    public void moveTo(Point vector) {
        centre.moveTo(vector);
    }

    public void moveIn(double x, double y) {
        centre.moveIn(x, y);
    }

    public void moveIn(Point destination) {
        centre.moveIn(destination);
    }
}
