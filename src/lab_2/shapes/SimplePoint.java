package lab_2.shapes;

public class SimplePoint implements Point {

    public SimplePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    private double x;
    private double y;

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void moveTo(double x, double y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }

    @Override
    public void moveTo(Point vector) {
        this.x = this.x + vector.getX();
        this.y = this.y + vector.getY();
    }

    @Override
    public void moveIn(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void moveIn(Point destination) {
        this.x = destination.getX();
        this.y = destination.getY();
    }


}

