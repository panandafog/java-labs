package lab_2.shapes;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Circle implements Ellipse {

    public Circle(SimplePoint centre, double diameter) throws IllegalArgumentException {
        if (diameter <= 0) {
            throw new IllegalArgumentException("Diameter of circle must be positive");
        }
        this.centre = centre;
        this.diameter = diameter;
    }

    protected double diameter;
    protected SimplePoint centre;

    public double getArea() {
        return (PI * pow(diameter / 2, 2));
    }

    public double getRotation() {
        return 0;
    }

    public void rotate(double degree) {
    }

    public double getPerimeter() {
        return (PI * diameter);
    }

    public void moveTo(double x, double y) {
        centre.moveIn(x, y);
    }

    public void moveTo(Point vector) {
        centre.moveTo(vector);
    }

    public void moveIn(double x, double y) {
        centre.moveIn(x, y);
    }

    public void moveIn(Point destination) {
        centre.moveIn(destination);
    }

}
