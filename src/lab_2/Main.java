package lab_2;

import lab_2.shapes.*;
import lab_2.shapes.Rectangle;
import lab_2.shapes.Shape;

import java.awt.*;
import java.util.BitSet;

public class Main {
    static double BiggestArea(Shape[] shapes, int size) {
        double biggestArea = 0;

        for (int i = 0; i < size; i++) {
            if (shapes[i].getArea() > biggestArea) {
                biggestArea = shapes[i].getArea();
            }
        }

        return biggestArea;
    }

    public static void main(String[] args) {
        Shape[] shapes = new Shape[]{
                new Circle(new SimplePoint(1, 1), 1),
                new Rectangle(new SimplePoint(1, 2), 1, 2),
                new Triangle(new SimplePoint(2, 2), new SimplePoint(2, 4), new SimplePoint(-1, -8)),
                new Circle(new SimplePoint(1, 1), 2),
                new Rectangle(new SimplePoint(7, 2), 4, 2),
                new Triangle(new SimplePoint(0, 2), new SimplePoint(2, 3), new SimplePoint(0, 0)),
                new Circle(new SimplePoint(-1, 1), 3),
                new Rectangle(new SimplePoint(-1, 2), 3, 3),
                new Triangle(new SimplePoint(1, 0), new SimplePoint(3, 3), new SimplePoint(-1, -4)),
                new Circle(new SimplePoint(3, 4), 0.5)
        };

        System.out.println(BiggestArea(shapes, 10));

    }
}