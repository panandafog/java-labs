package course.controller.response;

public class WarehouseNumberResponse extends SimpleResponse {
    private long warehouseNumber;

    public WarehouseNumberResponse(long warehouseNumber, Boolean success, String error) {
        super(success, error);
        this.warehouseNumber = warehouseNumber;
    }

    public WarehouseNumberResponse(long warehouseNumber, Boolean success) {
        super(success);
        this.warehouseNumber = warehouseNumber;
    }

    public long getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseNumber(long warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }
}
