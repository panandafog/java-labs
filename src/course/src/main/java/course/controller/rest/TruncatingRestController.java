package course.controller.rest;

import course.controller.response.SimpleResponse;
import course.service.GoodService;
import course.service.SaleService;
import course.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/truncate")
public class TruncatingRestController {
    @Autowired
    private GoodService goodService;
    @Autowired
    private SaleService saleService;
    @Autowired
    private WarehouseService warehouseService;

    @PostMapping(value = "/sales")
    public SimpleResponse truncateSales() {
        try {
            saleService.truncate();
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/warehouses")
    public SimpleResponse truncateWarehouses() {
        try {
            warehouseService.truncate();
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/all")
    public SimpleResponse truncateAllTables() {
        saleService.truncate();
        warehouseService.truncate();
        goodService.forceTruncate();
        ;
        return new SimpleResponse(true);
    }
}
