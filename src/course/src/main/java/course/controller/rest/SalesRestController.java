package course.controller.rest;

import course.controller.response.SimpleResponse;
import course.entity.Sale;
import course.service.SaleService;
import course.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SalesRestController {
    @Autowired
    private SaleService service;
    @Autowired
    private WarehouseService warehouseService;

    @PostMapping(value = "/add")
    public SimpleResponse addOrUpdateSale(@RequestBody Sale sale) {
        try {
            if (sale.getId() == null) {
                service.addOrUpdate(new Sale(sale.getGood_id(), sale.getGood_count()));
            } else {
                service.addOrUpdate(sale);
            }
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Такая запись о продаже уже существует");
        } catch (JpaObjectRetrievalFailureException ex) {
            return new SimpleResponse(false, "Такой товар не существует");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/sale")
    public SimpleResponse sale(@RequestBody Sale sale) {
        try {
            warehouseService.addToCountByIDAndWarehouseNumber(sale.getGood_id(), -sale.getGood_count(), sale.getWarehouse_number());
            service.addOrUpdate(new Sale(sale.getGood_id(), sale.getGood_count()));
        } catch (NullPointerException ex) {
            return new SimpleResponse(false, "Такого товара нет на складе");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/delete/by/id")
    public SimpleResponse deleteSaleById(@RequestBody Sale sale) {
        try {
            service.deleteById(sale.getId());
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Эта запись связана с другими таблицами");
        } catch (EmptyResultDataAccessException ex) {
            return new SimpleResponse(false, "Такая запись о продаже не существует");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/delete/all")
    public SimpleResponse deleteAllSales() {
        try {
            service.deleteAll();
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Сначала очистите другие таблицы");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public List<Sale> getAllSales() {
        List<Sale> tmp = null;
        try {
            tmp = service.getAll();
        } catch (Exception ex) {
            System.out.println("Exception in get all");
        }
        return tmp;
    }
}
