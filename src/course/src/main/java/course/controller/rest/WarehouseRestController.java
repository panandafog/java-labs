package course.controller.rest;

import course.controller.response.SimpleResponse;
import course.controller.response.WarehouseNumberResponse;
import course.entity.Rack;
import course.entity.Sale;
import course.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/warehouses")
public class WarehouseRestController {
    @Autowired
    private WarehouseService service;

    @PostMapping(value = "/set/warehouse")
    public SimpleResponse setWarehouse(@RequestBody Rack rack) {
        service.setWarehouseNum(rack.getWarehouse_number());
        return new SimpleResponse(true);
    }

    @RequestMapping(value = "/get/warehouse", method = RequestMethod.GET)
    public WarehouseNumberResponse getWarehouse() {
        return new WarehouseNumberResponse(service.getCurrentWarehouse(), true);
    }

    @PostMapping(value = "/add")
    public SimpleResponse addOrUpdateRack(@RequestBody Rack rack) {
        try {
            if (rack.getId() == null) {
                service.addOrUpdate(rack.getGood_id(), rack.getGood_count(), rack.getWarehouse_number());
            } else {
                service.addOrUpdate(rack);
            }
        } catch (DataIntegrityViolationException | JpaObjectRetrievalFailureException ex) {
            return new SimpleResponse(false, "Такой товар не существует");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }


    @PostMapping(value = "/add/knownnum")
    public SimpleResponse addRackKnownNum(@RequestBody Rack rack) {
        try {
            service.addOrUpdate(rack.getGood_id(), rack.getGood_count());
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Такой товар не существует");
        }

        return new SimpleResponse(true);
    }

    @PostMapping(value = "/add/to/count/by/id/and/warehouse")
    public SimpleResponse addToCountByIDAndWarehouseNumber(@RequestBody Sale sale) {
        try {
            service.addToCountByIDAndWarehouseNumber(sale.getGood_id(), sale.getGood_count(), sale.getWarehouse_number());
        } catch (EmptyResultDataAccessException ex) {
            return new SimpleResponse(false, "Такая запись на складе не существует");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/delete/by/id")
    public SimpleResponse deleteRackById(@RequestBody Rack rack) {
        try {
            service.deleteById(rack.getId());
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Эта запись связана с другими таблицами");
        } catch (EmptyResultDataAccessException ex) {
            return new SimpleResponse(false, "Такая запись на складах не существует");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @PostMapping(value = "/delete/all")
    public SimpleResponse deleteAllRacks() {
        try {
            service.deleteAll();
        } catch (DataIntegrityViolationException ex) {
            return new SimpleResponse(false, "Сначала очистите другие таблицы");
        } catch (Exception ex) {
            return new SimpleResponse(false, "Неизвестная ошибка");
        }
        return new SimpleResponse(true);
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public List<Rack> getAllRacks() {
        List<Rack> tmp = null;
        try {
            tmp = service.getAll();
        } catch (Exception ex) {
            System.out.println("Exception in get all");
        }
        return tmp;
    }

    @RequestMapping(value = "/get/by/warehouse/knownnum", method = RequestMethod.GET)
    public List<Rack> getAllFromSelectedWarehouse() {
        List<Rack> tmp = null;
        try {
            tmp = service.getBySelectedWarehouseNumber();
        } catch (Exception ex) {
            System.out.println("Exception in get all by warehouse");
        }
        return tmp;
    }

    @RequestMapping(value = "/get/by/warehouse", method = RequestMethod.GET)
    public List<Rack> getAllFromSelectedWarehouse(@RequestParam long warehouseNumber) {
        List<Rack> tmp = null;
        try {
            tmp = service.getByWarehouseNumber(warehouseNumber);
        } catch (Exception ex) {
            System.out.println("Exception in get all by warehouse");
        }
        return tmp;
    }
}
