package course.repository;

import course.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SaleRepository extends JpaRepository<Sale, Long> {
    @Modifying
    @Query(value = "truncate table sales", nativeQuery = true)
    void truncate();
}