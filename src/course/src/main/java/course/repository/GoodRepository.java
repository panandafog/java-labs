package course.repository;

import course.entity.Good;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GoodRepository extends JpaRepository<Good, Long> {
    @Query("select b from Good b where b.name = :name")
    Good findByName(@Param("name") String name);

    @Modifying
    @Query(value = "truncate table goods;", nativeQuery = true)
    void truncate();

    @Modifying
    @Query(value = "SET FOREIGN_KEY_CHECKS = :checks", nativeQuery = true)
    void setForeignKeyChecks(@Param("checks") int checks);
}
