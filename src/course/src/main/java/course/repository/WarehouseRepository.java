package course.repository;

import course.entity.Rack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WarehouseRepository extends JpaRepository<Rack, Long> {
    @Query("select b from Rack b where b.good.id = :goodID")
    Rack findByGoodID(@Param("goodID") long goodID);

    @Query("select b from Rack b where b.warehouseNumber = :warehouse_number")
    List<Rack> findByWarehouseNumber(@Param("warehouse_number") long warehouseNumber);

    @Query("select b from Rack b where b.good.id = :goodID and b.warehouseNumber = :warehouse_number")
    Rack findByGoodIDAndWarehouseNumber(@Param("goodID") long goodID, @Param("warehouse_number") long warehouseNumber);

    @Modifying
    @Query(value = "truncate table warehouses", nativeQuery = true)
    void truncate();
}
