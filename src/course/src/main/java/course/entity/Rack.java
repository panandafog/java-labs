package course.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "warehouses")
public class Rack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Good good;
    @Column(name = "good_count", nullable = false)
    private Long goodCount;
    @Column(name = "warehouse_number", nullable = false)
    private Long warehouseNumber;

    public Long getWarehouse_number() {
        return warehouseNumber;
    }

    public void setWarehouse_number(Long warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public Rack() {
    }

    public Rack(long goodId, long goodCount, long warehouseNumber) {
        this.goodCount = goodCount;
        this.good = new Good();
        this.warehouseNumber = warehouseNumber;
        this.good.setId(goodId);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long getGood_id() {
        return good.getId();
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public Long getGood_count() {
        return goodCount;
    }

    public void setGood_count(long goodCount) {
        this.goodCount = goodCount;
    }
}
