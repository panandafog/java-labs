package course.service;

import course.entity.Rack;
import course.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class WarehouseService {
    @Autowired
    private WarehouseRepository repository;
    private Long currentWarehouse = 1L;

    public Long getCurrentWarehouse() {
        return currentWarehouse;
    }

    public void setWarehouseNum(Long num) {
        this.currentWarehouse = num;
    }

    public void addOrUpdate(Rack rack) {
        repository.saveAndFlush(rack);
    }

    public void addOrUpdate(Long goodId, Long goodCount, Long warehouseNumber) {
        if (repository.findByGoodIDAndWarehouseNumber(goodId, warehouseNumber) != null) {
            addToCountByIDAndWarehouseNumber(goodId, goodCount, warehouseNumber);
        } else {
            repository.saveAndFlush(new Rack(goodId, goodCount, warehouseNumber));
        }
    }

    public void addOrUpdate(Long goodId, Long goodCount) {
        if (repository.findByGoodIDAndWarehouseNumber(goodId, currentWarehouse) != null) {
            addToCountByIDAndWarehouseNumber(goodId, goodCount, currentWarehouse);
        } else {
            repository.saveAndFlush(new Rack(goodId, goodCount, currentWarehouse));
        }
    }

    public void addToCountByID(Long goodId, Long goodCount) {
        Rack tmp = repository.findByGoodIDAndWarehouseNumber(goodId, currentWarehouse);
        tmp.setGood_count(tmp.getGood_count() + goodCount);
        repository.saveAndFlush(tmp);
    }

    public void addToCountByIDAndWarehouseNumber(Long goodId, Long goodCount, Long warehouseNumber) {
        Rack tmp = repository.findByGoodIDAndWarehouseNumber(goodId, warehouseNumber);
        tmp.setGood_count(tmp.getGood_count() + goodCount);
        repository.saveAndFlush(tmp);
    }

    public Rack getById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public void delete(Rack rack) {
        repository.delete((Rack) rack);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    @Transactional
    public void truncate() {
        repository.truncate();
    }

    public List<Rack> getAll() {
        return repository.findAll();
    }

    public List<Rack> getBySelectedWarehouseNumber() {
        if (currentWarehouse == 0) {
            return repository.findAll();
        }
        return repository.findByWarehouseNumber(currentWarehouse);
    }

    public List<Rack> getByWarehouseNumber(Long warehouseNumber) {
        if (warehouseNumber == 0) {
            return repository.findAll();
        }
        return repository.findByWarehouseNumber(warehouseNumber);
    }
}
