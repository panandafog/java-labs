package course.service;

import course.entity.Sale;
import course.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaleService {
    @Autowired
    private SaleRepository repository;

    public void addOrUpdate(Sale sale) {
        repository.saveAndFlush(sale);
    }

    public Sale getById(long id) {
        return repository.findById(id).orElse(null);
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public void delete(Sale sale) {
        repository.delete(sale);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    @Transactional
    public void truncate() {
        repository.truncate();
    }

    public List<Sale> getAll() {
        List<Sale> tmp = repository.findAll();
        return repository.findAll();
    }
}
