package course.service;

import course.entity.Good;
import course.repository.GoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GoodService {
    @Autowired
    private GoodRepository repository;

    public void addOrUpdate(Good good) {
        repository.saveAndFlush(good);
    }

    public Good getById(long id) {
        return repository.findById(id).orElse(null);
    }

    public Good getByName(String name) {
        return repository.findByName(name);
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public void deleteByName(String name) {
        deleteById(getByName(name).getId());
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public void delete(Good good) {
        repository.delete(good);
    }

    @Transactional
    public void truncate() {
        repository.truncate();
    }

    @Transactional
    public void forceTruncate() {
        repository.setForeignKeyChecks(0);
        repository.truncate();
        repository.setForeignKeyChecks(1);
    }

    public List<Good> getAll() {
        List<Good> tmp = repository.findAll();
        return repository.findAll();
    }
}
