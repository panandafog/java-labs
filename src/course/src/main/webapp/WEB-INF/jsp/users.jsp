<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Log in with your account</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/table.css">
</head>

<body>
<div>
    <h3>Список всех пользователь</h3>
    <table id='users'>
        <tbody>
        <th>ID</th>
        <th>UserName</th>
        <th>Password</th>
        <th>Role</th>
        <th width="1"></th>
        </tbody>
    </table>
    <a href="/" class="button gray">Главная</a>
</div>

<script>
    window.onload = function initTable() {
        let tableData;
        let table = document.getElementById('users');

        fetch('/users/get/all', {
            method: 'get'
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Get users request succeeded with JSON response', data);
                tableData = data;
                let tbody = table.getElementsByTagName("TBODY")[0];

                for (let index = 0; index < data.length; index++) {
                    let row = document.createElement("TR")
                    let td1 = document.createElement("TD")
                    td1.appendChild(document.createTextNode(data[index].id))
                    let td2 = document.createElement("TD")
                    td2.appendChild(document.createTextNode(data[index].username))
                    let td3 = document.createElement("TD")
                    td3.appendChild(document.createTextNode(data[index].password))
                    let td4 = document.createElement("TD")
                    td4.appendChild(document.createTextNode(data[index].roles[0].name))
                    let td5 = document.createElement("BUTTON")
                    let text = document.createTextNode("Delete");
                    td5.appendChild(text);
                    td5.onclick = function Delete(){
                        let user = {
                            id: tableData[index].id
                        };
                        fetch('/users/delete/by/id', {
                            method: 'post',
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                            },
                            body: JSON.stringify(user)
                        })
                            .then(res => res.json())
                            .then(function (data) {
                                console.log('Delete user request succeeded with JSON response', data);
                                if (data.success === false) {
                                    alert(data.error)
                                } else {
                                    document.location.reload()
                                }
                            })
                            .catch(function (error) {
                                console.error('Request FAILED ', error);
                            });
                    }

                    row.appendChild(td1);
                    row.appendChild(td2);
                    row.appendChild(td3);
                    row.appendChild(td4);
                    row.appendChild(td5);
                    tbody.appendChild(row);
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>

</body>
</html>