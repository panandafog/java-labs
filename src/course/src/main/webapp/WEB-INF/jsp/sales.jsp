<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Продажи</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/table.css">
</head>
<body>
<div>
    <h2>Управление продажами</h2>
</div>

<div>
    <h3>Продать со склада</h3>
    <div>
        <div>
            <input id="sale_good_id" name="goodId" type="number" placeholder="good ID"
                   autofocus="true"/>
        </div>
        <div>
            <input id="sale_good_count" name="goodCount" type="number" placeholder="good count"/>
        </div>
        <div>
            <input id="sale_warehouse_number" name="warehouseNumber" type="number" placeholder="warehouse number"/>
        </div>
        <div>
            <button id="sale_button" type="submit">Sale</button>
        </div>
    </div>

    <h3>Добавить запись о продаже</h3>
    <div>
        <div>
            <input id="add_good_id" name="goodId" type="number" placeholder="good ID"
                   autofocus="true"/>
        </div>
        <div>
            <input id="add_good_count" name="goodCount" type="number" placeholder="good count"/>
        </div>
        <div>
            <button id="add_button" type="submit">Add</button>
        </div>
    </div>

    <h3>Удалить запись о продаже</h3>
    <div>
        <div>
            <input id="delete_id" name="id" type="number" placeholder="id"
                   autofocus="true"/>
        </div>
        <div>
            <button id="delete_button" type="submit">Delete</button>
        </div>
    </div>

    <h3>Удалить все записи о продажах</h3>
    <div>
        <div>
            <button id="delete_all_button" type="submit">Delete</button>
        </div>
    </div>

</div>

<div>
    <h3>Список всех записей о продажах</h3>

    <select id="sort_column_select">
        <option disabled selected value> -- Сортировать по -- </option>
        <option value="1">ID</option>
        <option value="2">Good ID</option>
        <option value="3">Good Name</option>
        <option value="4">Good Count</option>
        <option value="5">Date</option>
    </select>
    <select id="sort_type_select">
        <option value="1">по возрастанию</option>
        <option value="2">по убыванию</option>
    </select>

    <table id='sales'>
        <tbody>
        <th>ID</th>
        <th>Good ID</th>
        <th>Good Name</th>
        <th>Good Count</th>
        <th>Date</th>
        <th width="1"></th>
        </tbody>
    </table>
    <a href="/" class="button gray">Главная</a>
</div>

<script>
    window.onkeydown = function (event) {
        if (event.key === 'Enter') {
            let edit = document.getElementById('edit');
            if (edit !== null) {
                edit.blur();
            }
        }
    }
</script>
<script>
    window.onload = function initTable() {
        let table = document.getElementById('sales');

        fetch('/sales/get/all', {
            method: 'get'
        })
            .then(res => res.json())
            .then(function (tableInitData) {
                console.log('Get sales request succeeded with JSON response', tableInitData);

                let tbody = table.getElementsByTagName("TBODY")[0];

                for (let index = 0; index < tableInitData.length; index++) {
                    let row = document.createElement("TR")
                    let td1 = document.createElement("TD")
                    td1.appendChild(document.createTextNode(tableInitData[index].id))
                    let td2 = document.createElement("TD")
                    td2.appendChild(document.createTextNode(tableInitData[index].good_id))
                    let td3 = document.createElement("TD")
                    td3.appendChild(document.createTextNode(tableInitData[index].good.name))
                    let td4 = document.createElement("TD")
                    td4.appendChild(document.createTextNode(tableInitData[index].good_count))
                    let td5 = document.createElement("TD")
                    td5.appendChild(document.createTextNode(tableInitData[index].create_date))
                    let td6 = document.createElement("BUTTON");
                    let text = document.createTextNode("Delete");
                    td6.appendChild(text);
                    td6.parentElement = row;
                    td6.onclick = function Delete(){
                        let sale = {
                            id: tableInitData[index].id
                        };
                        fetch('/sales/delete/by/id', {
                            method: 'post',
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                            },
                            body: JSON.stringify(sale)
                        })
                            .then(res => res.json())
                            .then(function (data) {
                                console.log('Delete sale request succeeded with JSON response', data);
                                if (data.success === false) {
                                    alert(data.error)
                                } else {
                                    row.remove();
                                }
                            })
                            .catch(function (error) {
                                console.error('Request FAILED ', error);
                            });
                    };

                    row.appendChild(td1);
                    row.appendChild(td2);
                    row.appendChild(td3);
                    row.appendChild(td4);
                    row.appendChild(td5);
                    row.appendChild(td6);
                    tbody.appendChild(row);

                    td2.onclick = function (element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "number");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function () {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: childValue
                            };

                            let sale = {
                                id: tableInitData[index].id,
                                good: good,
                                good_count: tableInitData[index].good_count,
                                create_date: tableInitData[index].create_date
                            };

                            let parentNode = this.parentNode;

                            fetch('/sales/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(sale)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add sale request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error);
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].good_id = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                    let newText = document.createTextNode(childValue);
                                    parentNode.appendChild(newText);
                                    parentNode.removeChild(parentNode.childNodes[0]);
                                });
                        }
                    };
                    td4.onclick = function (element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "number");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function () {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: tableInitData[index].good_id
                            };

                            let sale = {
                                id: tableInitData[index].id,
                                good: good,
                                good_count: childValue,
                                create_date: tableInitData[index].create_date
                            };

                            let parentNode = this.parentNode;

                            fetch('/sales/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(sale)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add sale request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error);
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].good_count = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                    let newText = document.createTextNode(childValue);
                                    parentNode.appendChild(newText);
                                    parentNode.removeChild(parentNode.childNodes[0]);
                                });
                        }
                    }
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('sale_button').onclick = function sale() {
        if (document.getElementById('sale_good_id').value.trim() === '' || document.getElementById('sale_good_count').value.trim() === '' || document.getElementById('sale_warehouse_number').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            id: document.getElementById('sale_good_id').value.trim()
        };

        let sale = {
            good: good,
            good_count: document.getElementById('sale_good_count').value.trim(),
            warehouse_number: document.getElementById('sale_warehouse_number').value.trim()
        };

        fetch('/sales/sale', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(sale)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Sale request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('add_button').onclick = function add() {
        if (document.getElementById('add_good_id').value.trim() === '' || document.getElementById('add_good_count').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            id: document.getElementById('add_good_id').value.trim()
        };

        let sale = {
            good: good,
            good_count: document.getElementById('add_good_count').value.trim()
        };

        fetch('/sales/add', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(sale)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Add sale request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_button').onclick = function deleteByID() {
        if (document.getElementById('delete_id').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let sale = {
            id: document.getElementById('delete_id').value.trim()
        };

        fetch('/sales/delete/by/id', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(sale)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete good request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_all_button').onclick = function deleteAll() {
        fetch('/sales/delete/all', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete all goods request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('sort_column_select').onchange = function () {
        sortSelect(document.getElementById('warehouses'), document.getElementById('sort_column_select').selectedIndex - 1, document.getElementById('sort_type_select').selectedIndex);
    };

    document.getElementById('sort_type_select').onchange = function () {
        sortSelect(document.getElementById('warehouses'), document.getElementById('sort_column_select').selectedIndex - 1, document.getElementById('sort_type_select').selectedIndex);
    };

    function sortSelect(table, index, type) {

        if (index === 0) {
            index = 1;
        }
        let sortedRows;
        if (type === 0) {
            sortedRows = Array.from(table.rows)
                .slice(1)
                .sort((rowA, rowB) => rowA.cells[index].innerHTML > rowB.cells[index].innerHTML ? 1 : -1);
        } else if (type === 1){
            sortedRows = Array.from(table.rows)
                .slice(1)
                .sort((rowA, rowB) => rowA.cells[index].innerHTML < rowB.cells[index].innerHTML ? 1 : -1);
        }
        table.tBodies[0].append(...sortedRows);
    }
</script>
<script>
    document.getElementById('sort_column_select').onchange = function () {
        let table = (document.getElementById('sales'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 2 || index === 4);
        sort(table, index - 1, sortType, isNumber);
    };

    document.getElementById('sort_type_select').onchange = function () {
        let table = (document.getElementById('sales'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 2 || index === 4);
        sort(table, index - 1, sortType, isNumber);
    };

    function sort(table, index, sortType, isNumber) {
        let sortedRows;
        if (sortType === 0) {
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML > 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML > rowB.cells[index].innerHTML ? 1 : -1);
            }
        } else if (sortType === 1){
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML < 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML < rowB.cells[index].innerHTML ? 1 : -1);
            }
        }
        table.tBodies[0].append(...sortedRows);
    }
</script>

</body>
</html>