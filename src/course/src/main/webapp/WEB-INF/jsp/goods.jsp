<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Товары</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/table.css">
</head>
<body>
<div>
    <h2>Управление товарами</h2>
</div>
<div>

    <h3>Добавить товар</h3>
    <div>
        <div>
            <input id="add_name" name="name" type="text" placeholder="Name"
                   autofocus="true"/>
        </div>
        <div>
            <input id="add_priority" name="priority" type="number" placeholder="Priority"/>
        </div>
        <div>
            <button id="add_button" type="submit">Add</button>
        </div>
    </div>

    <h3>Удалить товар по ID</h3>
    <div>
        <div>
            <input id="delete_id" name="id" type="number" placeholder="id"
                   autofocus="true"/>
        </div>
        <div>
            <button id="delete_button" type="submit">Delete</button>
        </div>
    </div>

    <h3>Удалить товар по названию</h3>
    <div>
        <div>
            <input id="delete_name" name="name" type="text" placeholder="name"
                   autofocus="true"/>
        </div>
        <div>
            <button id="delete_by_name_button" type="submit">Delete</button>
        </div>
    </div>

    <h3>Удалить все товары</h3>
    <div>
        <div>
            <button id="delete_all_button" type="submit">Delete</button>
        </div>
    </div>

</div>
<div>
    <h3>Список всех товаров</h3>

        <select id="sort_column_select">
            <option disabled selected value> -- Сортировать по -- </option>
            <option value="1">ID</option>
            <option value="2">Name</option>
            <option value="3">Priority</option>
        </select>
        <select id="sort_type_select">
            <option value="1">по возрастанию</option>
            <option value="2">по убыванию</option>
        </select>

    <table id='goods'>
        <tbody>
        <th>ID</th>
        <th>Name</th>
        <th>Priority</th>
        <th width="1"></th>
        </tbody>
    </table>
    <a href="/" class="button gray">Главная</a>
</div>

<script>
    window.onkeydown = function(event){
        if (event.key === 'Enter') {
            let edit = document.getElementById('edit');
            if (edit !== null) {
                edit.blur();
            }
        }
    }
</script>
<script>
    window.onload = function initTable() {
        let table = document.getElementById('goods');

        fetch('/goods/get/all', {
            method: 'get'
        })
            .then(res => res.json())
            .then(function (tableInitData) {
                console.log('Get goods request succeeded with JSON response', tableInitData);

                let tbody = table.getElementsByTagName("TBODY")[0];

                for (let index = 0; index < tableInitData.length; index++) {
                    let row = document.createElement("TR")
                    let td1 = document.createElement("TD")
                    td1.appendChild(document.createTextNode(tableInitData[index].id))
                    let td2 = document.createElement("TD")
                    td2.appendChild(document.createTextNode(tableInitData[index].name))
                    let td3 = document.createElement("TD")
                    td3.appendChild(document.createTextNode(tableInitData[index].priority))
                    let td4 = document.createElement("BUTTON");
                    td4.align="center"; td4.valign="center";
                    let text = document.createTextNode("Delete");
                    td4.appendChild(text);
                    td4.parentElement = row;
                    td4.onclick = function Delete(){
                        let good = {
                            id: tableInitData[index].id
                        };
                        fetch('/goods/delete/by/id', {
                            method: 'post',
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                            },
                            body: JSON.stringify(good)
                        })
                            .then(res => res.json())
                            .then(function (data) {
                                console.log('Delete sale request succeeded with JSON response', data);
                                if (data.success === false) {
                                    alert(data.error)
                                } else {
                                    row.remove();
                                }
                            })
                            .catch(function (error) {
                                console.error('Request FAILED ', error);
                            });
                    };

                    row.appendChild(td1);
                    row.appendChild(td2);
                    row.appendChild(td3);
                    row.appendChild(td4);
                    tbody.appendChild(row);

                    td2.onclick = function(element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "text");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function() {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: tableInitData[index].id,
                                name: childValue,
                                priority: tableInitData[index].priority
                            };

                            let parentNode = this.parentNode;

                            fetch('/goods/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(good)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add good request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error);
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].name = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                let newText = document.createTextNode(childValue);
                                parentNode.appendChild(newText);
                                parentNode.removeChild(parentNode.childNodes[0]);
                            });
                        }
                    };
                    td3.onclick = function(element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "number");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function() {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: tableInitData[index].id,
                                name: tableInitData[index].name,
                                priority: childValue
                            };

                            let parentNode = this.parentNode;

                            fetch('/goods/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(good)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add good request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error)
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].priority = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                    let newText = document.createTextNode(childValue);
                                    parentNode.appendChild(newText);
                                    parentNode.removeChild(parentNode.childNodes[0]);
                                });
                        }
                    }
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });

    }
</script>
<script>
    document.getElementById('add_button').onclick = function add() {
        if (document.getElementById('add_name').value.trim() === '' || document.getElementById('add_priority').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            name: document.getElementById('add_name').value.trim(),
            priority: document.getElementById('add_priority').value.trim()
        };

        fetch('/goods/add', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(good)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Add good request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_button').onclick = function deleteByID() {
        if (document.getElementById('delete_id').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            id: document.getElementById('delete_id').value.trim()
        };

        fetch('/goods/delete/by/id', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(good)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete by ID request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_by_name_button').onclick = function deleteByID() {
        if (document.getElementById('delete_name').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            name: document.getElementById('delete_name').value.trim()
        };

        fetch('/goods/delete/by/name', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(good)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete by name request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_all_button').onclick = function deleteAll() {
        fetch('/goods/delete/all', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete all goods request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('sort_column_select').onchange = function () {
        let table = (document.getElementById('goods'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 3);
        sort(table, index - 1, sortType, isNumber);
    };

    document.getElementById('sort_type_select').onchange = function () {
        let table = (document.getElementById('doods'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 3);
        sort(table, index - 1, sortType, isNumber);
    };

    function sort(table, index, sortType, isNumber) {
        let sortedRows;
        if (sortType === 0) {
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML > 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML > rowB.cells[index].innerHTML ? 1 : -1);
            }
        } else if (sortType === 1){
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML < 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML < rowB.cells[index].innerHTML ? 1 : -1);
            }
        }
        table.tBodies[0].append(...sortedRows);
    }
</script>

</body>
</html>