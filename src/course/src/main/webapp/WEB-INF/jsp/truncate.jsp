<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Очистка таблиц</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/table.css">
</head>
<body>

<div><h1>Очистка таблиц</h1></div>

<div>
    <a id="truncate_sales" class="button brown">Продажи</a>
</div>
<div>
    <a id="truncate_warehouses" class="button brown">Склады</a>
</div>
<div>
    <a id="truncate_all" class="button red">Все таблицы</a>
</div>
<div>
    <a href="/" class="button gray">Главная</a>
</div>
<script>
    document.getElementById('truncate_sales').onclick = function truncateAll() {
        fetch('/truncate/sales', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Truncate sales request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('truncate_warehouses').onclick = function truncateAll() {
        fetch('/truncate/warehouses', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Truncate warehouses request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('truncate_all').onclick = function truncateAll() {
        fetch('/truncate/all', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Truncate all request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>

</body>
</html>