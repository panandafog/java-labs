<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
</head>
<body>

<div><h1>Opt Firm Manager</h1></div>
<div>
    <h3>${pageContext.request.userPrincipal.name}</h3>
    <sec:authorize access="!isAuthenticated()">
        <a href="/login" class="button gray">Войти</a>
    </sec:authorize>
</div>
<div>
    <sec:authorize access="!isAuthenticated()">
        <a href="/registration" class="button gray">Зарегистрироваться</a>
    </sec:authorize>
</div>
<div>
    <sec:authorize access="isAuthenticated()">
        <a href="/logout" class="button gray">Выйти</a>
    </sec:authorize>
</div>
<div>
    <a href="/goods" class="button gray">Товары</a>
</div>
<div>
    <a href="/sales" class="button gray">Продажи</a>
</div>
<div>
    <a href="/warehouses" class="button gray">Склады</a>
</div>
<div>
    <sec:authorize access="hasAuthority('ADMIN')">
        <a href="/truncate" class="button brown">Очистка таблиц</a>
    </sec:authorize>
</div>
<div>
    <sec:authorize access="hasAuthority('ADMIN')">
        <a href="/users" class="button red">Пользователи</a>
    </sec:authorize>
</div>

</body>
</html>