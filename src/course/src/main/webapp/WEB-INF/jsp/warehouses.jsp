<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Склады</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/table.css">
</head>
<body>
<div>
    <h2>Управление складами</h2>
</div>

<div>
    <h3>Выберите склад</h3>
    <form>
        <select id="warehouse_select">
            <option value="1">Склад 1</option>
            <option value="2">Склад 2</option>
        </select>
    </form>
</div>

<div>
    <h3>Добавить товар на склад</h3>
    <div>
        <div>
            <input id="add_good_id" name="goodId" type="number" placeholder="good ID"
                   autofocus="true"/>
        </div>
        <div>
            <input id="add_good_count" name="goodCount" type="number" placeholder="good count"/>
        </div>
        <div>
            <button id="add_button" type="submit">Add</button>
        </div>
    </div>

    <h3>Удалить товар</h3>
    <div>
        <div>
            <input id="delete_id" name="id" type="number" placeholder="id"
                   autofocus="true"/>
        </div>
        <div>
            <button id="delete_button" type="submit">Delete</button>
        </div>
    </div>

    <h3>Удалить все товары со склада</h3>
    <div>
        <div>
            <button id="delete_all_button" type="submit">Delete</button>
        </div>
    </div>
</div>
<div>
    <h3>Список всех товаров на складе</h3>

    <select id="sort_column_select">
        <option disabled selected value> -- Сортировать по -- </option>
        <option value="1">ID</option>
        <option value="2">Good ID</option>
        <option value="3">Good Name</option>
        <option value="4">Good Count</option>
    </select>
    <select id="sort_type_select">
        <option value="1">по возрастанию</option>
        <option value="2">по убыванию</option>
    </select>

    <table id="warehouses">
        <tbody>
        <th>ID</th>
        <th>Good ID</th>
        <th>Good Name</th>
        <th>Good Count</th>
        <th>Warehouse</th>
        <th width="1"></th>
        </tbody>
    </table>
    <a href="/" class="button gray">Главная</a>
</div>

<script>
    window.onkeydown = function(event){
        if (event.key === 'Enter') {
            let edit = document.getElementById('edit');
            if (edit !== null) {
                edit.blur();
            }
        }
    }
</script>
<script>
    window.onload = function init() {

        fetch('/warehouses/get/warehouse', {
            method: 'get'
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Get warehouse request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    let select = document.getElementById('warehouse_select');
                    select.selectedIndex = data.warehouseNumber - 1;
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });


        let table = document.getElementById('warehouses');

        fetch('/warehouses/get/by/warehouse/knownnum', {
            method: 'get'
        })
            .then(res => res.json())
            .then(function(tableInitData) {
                console.log('Get warehouses request succeeded with JSON response', tableInitData);

                let tbody = table.getElementsByTagName("TBODY")[0];

                for (let index = 0; index < tableInitData.length; index++) {
                    let row = document.createElement("TR")
                    let td1 = document.createElement("TD")
                    td1.appendChild(document.createTextNode(tableInitData[index].id))
                    let td2 = document.createElement("TD")
                    td2.appendChild(document.createTextNode(tableInitData[index].good_id))
                    let td3 = document.createElement("TD")
                    td3.appendChild(document.createTextNode(tableInitData[index].good.name))
                    let td4 = document.createElement("TD")
                    td4.appendChild(document.createTextNode(tableInitData[index].good_count))
                    let td5 = document.createElement("TD")
                    td5.appendChild(document.createTextNode(tableInitData[index].warehouse_number))
                    let td6 = document.createElement("BUTTON");
                    let text = document.createTextNode("Delete");
                    td6.appendChild(text);
                    td6.parentElement = row;
                    td6.onclick = function Delete(){
                        let rack = {
                            id: tableInitData[index].id
                        };
                        fetch('/warehouses/delete/by/id', {
                            method: 'post',
                            headers: {
                                'Content-Type': 'application/json;charset=utf-8'
                            },
                            body: JSON.stringify(rack)
                        })
                            .then(res => res.json())
                            .then(function (data) {
                                console.log('Delete sale request succeeded with JSON response', data);
                                if (data.success === false) {
                                    alert(data.error)
                                } else {
                                    row.remove();
                                }
                            })
                            .catch(function (error) {
                                console.error('Request FAILED ', error);
                            });
                    };

                    row.appendChild(td1);
                    row.appendChild(td2);
                    row.appendChild(td3);
                    row.appendChild(td4);
                    row.appendChild(td5);
                    row.appendChild(td6);
                    tbody.appendChild(row);

                    td2.onclick = function(element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "number");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function() {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: childValue,
                            };

                            let rack = {
                                id: tableInitData[index].id,
                                good: good,
                                good_count: tableInitData[index].good_count,
                                warehouse_number: tableInitData[index].warehouse_number
                            };

                            let parentNode = this.parentNode;

                            fetch('/warehouses/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(rack)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add rack request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error);
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].good_id = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                let newText = document.createTextNode(childValue);
                                parentNode.appendChild(newText);
                                parentNode.removeChild(parentNode.childNodes[0]);
                            });
                        }
                    };
                    td4.onclick = function(element) {
                        let target = element.target;
                        let elementName = target.tagName.toLowerCase();
                        if (elementName === "input") {
                            return false;
                        }

                        let parentNode = this.childNodes[0];
                        let parentValue = parentNode.nodeValue;
                        this.removeChild(this.childNodes[0]);
                        let input = document.createElement("input");
                        this.appendChild(input);
                        input.setAttribute("type", "number");
                        input.setAttribute("id", "edit");
                        input.setAttribute("value", parentValue);
                        input.focus();

                        input.onblur = function() {
                            let childValue = this.value.trim();
                            if (childValue === '') {
                                alert('поле не может быть пустым');
                                let newText = document.createTextNode(parentValue);
                                this.parentNode.appendChild(newText);
                                this.parentNode.removeChild(this.parentNode.childNodes[0]);
                                return;
                            }

                            let good = {
                                id: tableInitData[index].good_id,
                            };

                            let rack = {
                                id: tableInitData[index].id,
                                good: good,
                                good_count: childValue,
                                warehouse_number: tableInitData[index].warehouse_number
                            };

                            let parentNode = this.parentNode;

                            fetch('/warehouses/add', {
                                method: 'post',
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                body: JSON.stringify(rack)
                            })
                                .then(res => res.json())
                                .then(function (data) {
                                    console.log('Add rack request succeeded with JSON response', data);
                                    if (data.success === false) {
                                        alert(data.error);
                                        childValue = parentValue;
                                    } else {
                                        tableInitData[index].good_count = childValue;
                                    }
                                })
                                .catch(function (error) {
                                    console.error('Request FAILED ', error);
                                    childValue = parentValue;
                                })
                                .finally(function () {
                                    let newText = document.createTextNode(childValue);
                                    parentNode.appendChild(newText);
                                    parentNode.removeChild(parentNode.childNodes[0]);
                                });
                        }
                    }
                }
            })
            .catch(function(error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('add_button').onclick = function add() {
        if (document.getElementById('add_good_id').value.trim() === '' || document.getElementById('add_good_count').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let good = {
            id: document.getElementById('add_good_id').value.trim()
        };

        let rack = {
            good: good,
            good_count: document.getElementById('add_good_count').value.trim()
        };

        fetch('/warehouses/add/knownnum', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(rack)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Add rack request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error);
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_button').onclick = function deleteByID() {
        if (document.getElementById('delete_id').value.trim() === '') {
            alert("поле не может быть пустым");
            return;
        }

        let rack = {
            id: document.getElementById('delete_id').value.trim()
        };

        fetch('/warehouses/delete/by/id', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(rack)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete rack request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('delete_all_button').onclick = function deleteAll() {
        fetch('/warehouses/delete/all', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: ""
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Delete all racks request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('warehouse_select').onchange = function warehouseSelect() {
        let number = + document.getElementById('warehouse_select').selectedIndex + 1;

        let rack = {
            warehouse_number: number
        };

        fetch('/warehouses/set/warehouse', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(rack)
        })
            .then(res => res.json())
            .then(function (data) {
                console.log('Select warehouse request succeeded with JSON response', data);
                if (data.success === false) {
                    alert(data.error)
                } else {
                    document.location.reload()
                }
            })
            .catch(function (error) {
                console.error('Request FAILED ', error);
            });
    }
</script>
<script>
    document.getElementById('sort_column_select').onchange = function () {
        let table = (document.getElementById('warehouses'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 2 || index === 4);
        sort(table, index - 1, sortType, isNumber);
    };

    document.getElementById('sort_type_select').onchange = function () {
        let table = (document.getElementById('warehouses'));
        let index = document.getElementById('sort_column_select').selectedIndex;
        let sortType = document.getElementById('sort_type_select').selectedIndex;
        let isNumber = (index === 1 || index === 2 || index === 4);
        sort(table, index - 1, sortType, isNumber);
    };

    function sort(table, index, sortType, isNumber) {
        let sortedRows;
        if (sortType === 0) {
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML > 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML > rowB.cells[index].innerHTML ? 1 : -1);
            }
        } else if (sortType === 1){
            if (isNumber){
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => (rowA.cells[index].innerHTML - rowB.cells[index].innerHTML < 0) ? 1 : -1);
            } else {
                sortedRows = Array.from(table.rows)
                    .slice(1)
                    .sort((rowA, rowB) => rowA.cells[index].innerHTML < rowB.cells[index].innerHTML ? 1 : -1);
            }
        }
        table.tBodies[0].append(...sortedRows);
    }
</script>

</body>
</html>