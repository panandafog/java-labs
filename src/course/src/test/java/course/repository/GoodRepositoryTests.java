package course.repository;

import course.entity.Good;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class GoodRepositoryTests {

    @Autowired
    GoodRepository repository;
    Good good = new Good("l12i3u4h1i2", 1);

    @Test
    void findByName() {
        repository.saveAndFlush(good);
        Good found = repository.findByName(good.getName());
        assertNotNull(found);
        assertEquals(found.getName(), good.getName());
        assertEquals(found.getPriority(), good.getPriority());
        repository.deleteById(good.getId());
    }

}