package course.repository;

import course.entity.Good;
import course.entity.Rack;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class WarehouseRepositoryTests {

    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    GoodRepository goodRepository;

    @Test
    void findByName() {
        Good good = new Good("jn85765asfsdfkkn", 1);
        goodRepository.saveAndFlush(good);
        Good foundGood = goodRepository.findByName(good.getName());
        assertNotNull(foundGood);
        good.setId(foundGood.getId());

        Rack rack = new Rack(good.getId(), 1, 2);
        warehouseRepository.saveAndFlush(rack);
        Rack foundRack = warehouseRepository.findByGoodIDAndWarehouseNumber(good.getId(), 2);
        assertNotNull(foundRack);

        assertEquals(foundRack.getGood_id(), good.getId());
        assertEquals(foundRack.getGood_count(), rack.getGood_count());
        assertEquals(foundRack.getWarehouse_number(), rack.getWarehouse_number());

        warehouseRepository.deleteById(rack.getId());
        goodRepository.deleteById(good.getId());
    }

}