package course.utility;

import course.entity.Good;
import course.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Component
public class GoodServiceUtility {

    @Autowired
    GoodService service;

    public Good getGood() {
        Good good = new Good(BasicUtility.getRandomString(), 1);

        service.addOrUpdate(good);
        Good gotGood = service.getByName(good.getName());
        assertNotNull(gotGood);
        good.setId(gotGood.getId());
        return good;
    }

    public void deleteAll() {
        service.deleteAll();
    }

    public void truncate() {
        service.forceTruncate();
    }

}
