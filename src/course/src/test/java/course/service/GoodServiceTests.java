package course.service;

import course.entity.Good;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class GoodServiceTests {

    @Autowired
    GoodService service;
    @Autowired
    GoodServiceUtility utility;

    @Test
    public void addAndGetValidation() {

        Good good = utility.getGood();

        service.addOrUpdate(good);

        Good gotGood = service.getByName(good.getName());
        assertNotNull(gotGood);
        assertEquals(good.getName(), gotGood.getName());
        assertEquals(good.getPriority(), gotGood.getPriority());

        good.setId(gotGood.getId());

        gotGood = service.getById(good.getId());
        assertNotNull(gotGood);
        assertEquals(good.getName(), gotGood.getName());
        assertEquals(good.getPriority(), gotGood.getPriority());

        assertFalse(service.getAll().isEmpty());

        utility.deleteAll();

    }

    @Test
    public void addAndDeleteValidation() {

        Good good = utility.getGood();

        service.addOrUpdate(good);
        assertNotNull(service.getByName(good.getName()));
        good.setId(service.getByName(good.getName()).getId());

        service.deleteById(good.getId());
        assertNull(service.getById(good.getId()));

        service.addOrUpdate(good);

        service.deleteByName(good.getName());
        assertNull(service.getByName(good.getName()));

        service.addOrUpdate(good);
        good.setId(service.getByName(good.getName()).getId());

        service.delete(good);
        assertNull(service.getByName(good.getName()));

        service.addOrUpdate(good);

        service.deleteAll();
        assertNull(service.getByName(good.getName()));
        utility.deleteAll();
    }

    @Test
    public void updateValidation(){
        Good good = utility.getGood();

        service.addOrUpdate(good);
        Good gotGood = service.getByName(good.getName());
        good.setPriority(good.getPriority() + 1);
        assertDoesNotThrow(() -> {service.addOrUpdate(good);});
        good.setId(gotGood.getId() + 1);
        assertThrows(Exception.class, () -> {service.addOrUpdate(good);});

        utility.deleteAll();
    }
}