package course.service;

import course.entity.Good;
import course.entity.Sale;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class SaleServiceTests {

    @Autowired
    SaleService saleService;
    @Autowired
    GoodService goodService;
    @Autowired
    GoodServiceUtility utility;

    @Test
    public void addAndGetValidation() {

        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Sale sale = new Sale(good.getId(), 1);

        int size = saleService.getAll().size();
        saleService.addOrUpdate(sale);

        assertFalse(saleService.getAll().isEmpty());
        assertEquals(saleService.getAll().size(), size + 1);

        Sale gotSale = saleService.getAll().get(size);
        assertNotNull(gotSale);
        assertEquals(sale.getGood_id(), gotSale.getGood_id());
        assertEquals(sale.getGood_count(), gotSale.getGood_count());

        sale.setId(gotSale.getId());

        gotSale = saleService.getById(sale.getId());
        assertNotNull(gotSale);
        assertEquals(sale.getId(), gotSale.getId());
        assertEquals(sale.getGood_id(), gotSale.getGood_id());
        assertEquals(sale.getGood_count(), gotSale.getGood_count());

        saleService.deleteById(sale.getId());
        utility.deleteAll();
    }

    @Test
    public void addAndDeleteValidation() {

        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Sale sale = new Sale(good.getId(), 1);

        int size = saleService.getAll().size();
        saleService.addOrUpdate(sale);
        Sale gotSale = saleService.getAll().get(size);
        assertNotNull(gotSale);
        sale.setId(gotSale.getId());

        saleService.deleteById(sale.getId());
        assertNull(saleService.getById(sale.getId()));

        size = saleService.getAll().size();
        saleService.addOrUpdate(sale);
        gotSale = saleService.getAll().get(size);
        assertNotNull(gotSale);
        sale.setId(gotSale.getId());

        saleService.delete(sale);
        assertNull(saleService.getById(sale.getId()));

        size = saleService.getAll().size();
        saleService.addOrUpdate(sale);
        gotSale = saleService.getAll().get(size);
        assertNotNull(gotSale);
        sale.setId(gotSale.getId());

        saleService.deleteAll();
        assertNull(saleService.getById(sale.getId()));
        utility.deleteAll();
    }

    @Test
    public void updateValidation(){
        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Sale sale = new Sale(good.getId(), 1);

        int size = saleService.getAll().size();
        saleService.addOrUpdate(sale);

        Sale gotSale = saleService.getAll().get(size);
        sale.setGood_count(sale.getGood_count() + 1);

        assertDoesNotThrow(() -> {saleService.addOrUpdate(sale);});
        sale.setId(gotSale.getId() + 1);
        assertDoesNotThrow(() -> {saleService.addOrUpdate(sale);});

        saleService.deleteAll();
        utility.deleteAll();
    }
}