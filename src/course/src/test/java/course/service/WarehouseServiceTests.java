package course.service;

import course.entity.Good;
import course.entity.Rack;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class WarehouseServiceTests {

    @Autowired
    WarehouseService warehouseService;
    @Autowired
    GoodService goodService;
    @Autowired
    GoodServiceUtility utility;

    long warehouseNumber = 1;

    @Test
    public void addAndGetValidation() {

        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Rack rack = new Rack(good.getId(), 1, warehouseNumber);

        int size = warehouseService.getAll().size();
        warehouseService.addOrUpdate(rack);

        assertFalse(warehouseService.getAll().isEmpty());
        assertEquals(warehouseService.getAll().size(), size + 1);

        Rack gotRack = warehouseService.getAll().get(size);
        assertNotNull(gotRack);
        assertEquals(rack.getGood_id(), gotRack.getGood_id());
        assertEquals(rack.getGood_count(), gotRack.getGood_count());
        assertEquals(rack.getWarehouse_number(), gotRack.getWarehouse_number());

        rack.setId(gotRack.getId());

        gotRack = warehouseService.getById(rack.getId());
        assertNotNull(gotRack);
        assertEquals(rack.getId(), gotRack.getId());
        assertEquals(rack.getGood_id(), gotRack.getGood_id());
        assertEquals(rack.getGood_count(), gotRack.getGood_count());
        assertEquals(rack.getWarehouse_number(), gotRack.getWarehouse_number());

        warehouseService.deleteById(rack.getId());
        utility.deleteAll();
    }

    @Test
    public void addAndDeleteValidation() {

        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Rack rack = new Rack(good.getId(), 1, warehouseNumber);

        int size = warehouseService.getAll().size();
        warehouseService.addOrUpdate(rack);
        Rack gotRack = warehouseService.getAll().get(size);
        assertNotNull(gotRack);
        rack.setId(gotRack.getId());

        warehouseService.deleteById(rack.getId());
        assertNull(warehouseService.getById(rack.getId()));

        size = warehouseService.getAll().size();
        warehouseService.addOrUpdate(rack);
        gotRack = warehouseService.getAll().get(size);
        assertNotNull(gotRack);
        rack.setId(gotRack.getId());

        warehouseService.delete(rack);
        assertNull(warehouseService.getById(rack.getId()));

        size = warehouseService.getAll().size();
        warehouseService.addOrUpdate(rack);
        gotRack = warehouseService.getAll().get(size);
        assertNotNull(gotRack);
        rack.setId(gotRack.getId());

        warehouseService.deleteAll();
        assertNull(warehouseService.getById(rack.getId()));
        utility.deleteAll();
    }

    @Test
    public void updateValidation(){
        Good good = utility.getGood();

        goodService.addOrUpdate(good);

        Good gotGood = goodService.getByName(good.getName());
        assertNotNull(gotGood);

        good.setId(gotGood.getId());

        Rack rack = new Rack(good.getId(), 1, warehouseNumber);

        int size = warehouseService.getAll().size();
        warehouseService.addOrUpdate(rack);
        rack.setGood_count(rack.getGood_count());

        assertDoesNotThrow(() -> {warehouseService.addOrUpdate(rack);});
        Rack gotRack = warehouseService.getAll().get(size);
        assertEquals(rack.getGood_count(), gotRack.getGood_count());

        warehouseService.addOrUpdate(rack);


        assertDoesNotThrow(() -> {warehouseService.addOrUpdate(rack.getGood_id(), rack.getGood_count(), rack.getWarehouse_number());});
        gotRack = warehouseService.getAll().get(size);
        assertEquals(rack.getGood_count() * 2, gotRack.getGood_count());

        warehouseService.deleteAll();
        utility.deleteAll();
    }
}