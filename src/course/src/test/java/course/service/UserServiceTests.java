package course.service;

import course.entity.Role;
import course.entity.User;
import course.utility.BasicUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTests {

    @Autowired
    UserService service;

    @Test
    public void basicValidation() {
        String password = BasicUtility.getRandomString();
        User user = new User(BasicUtility.getRandomString(), password, password);
        int originalSize = service.getAllUsers().size();

        service.addUser(user);
        assertEquals(service.getAllUsers().size(), originalSize + 1);
        user.setId(service.getAllUsers().get(originalSize).getId());

        UserDetails gotUserDetails = service.loadUserByUsername(user.getUsername());
        assertNotNull(gotUserDetails);
        assertEquals(user.getUsername(), gotUserDetails.getUsername());

        User gotUser = service.getUserById(user.getId());
        assertNotNull(gotUser);
        assertEquals(user.getUsername(), gotUser.getUsername());
        assertEquals(user.getId(), gotUser.getId());
        assertEquals(gotUser.getRoles().size(), user.getRoles().size());

        Role[] rhs = new Role[gotUser.getRoles().size()];
        user.getRoles().toArray(rhs);
        Role[] lhs = new Role[gotUser.getRoles().size()];
        gotUser.getRoles().toArray(lhs);

        for (int index = 0; index < gotUser.getRoles().size(); index++) {
            assertEquals(lhs[index].getName(), rhs[index].getName());
            assertEquals(lhs[index].getId(), rhs[index].getId());
        }
        service.deleteUser(user.getId());
        assertEquals(service.getAllUsers().size(), originalSize);
    }
}