package course.controller.rest;

import course.entity.User;
import course.utility.BasicUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class UsersAndRegistrationRestControllersTests {

    @Autowired
    RegistrationRestController registrationController;
    @Autowired
    UsersRestController usersController;

    @Test
    public void basicValidation() {
        String password = BasicUtility.getRandomString();
        User user = new User(BasicUtility.getRandomString(), password, password);
        int originalSize = usersController.getAll().size();

        registrationController.register(user);
        assertEquals(usersController.getAll().size(), originalSize + 1);
        assertEquals(usersController.getAll().get(originalSize).getUsername(), user.getUsername());

        user.setId(usersController.getAll().get(originalSize).getId());

        usersController.delete(user);
        assertEquals(usersController.getAll().size(), originalSize);
    }

}