package course.controller.rest;

import course.entity.Good;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class GoodsRestControllerTests {

    @Autowired
    GoodsRestController controller;
    @Autowired
    GoodServiceUtility utility;

    @Test
    public void addAndDeleteAllValidation() {
        int size = controller.getAllGoods().size();
        Good good1 = utility.getGood();
        Good good2 = utility.getGood();

        controller.addOrUpdateGood(good1);
        controller.addOrUpdateGood(good2);
        assertEquals(controller.getAllGoods().size(), size + 2);

        controller.deleteAllGoods();
        assertEquals(controller.getAllGoods().size(), 0);
        utility.deleteAll();
    }

    @Test
    public void deleteGoodByIdValidation() {
        int size = controller.getAllGoods().size();
        Good good = utility.getGood();

        controller.addOrUpdateGood(good);
        assertEquals(controller.getAllGoods().size(), size + 1);
        good.setId(controller.getAllGoods().get(size).getId());

        Good goodToDelete = new Good();
        goodToDelete.setId(good.getId());
        controller.deleteGoodById(goodToDelete);
        assertEquals(controller.getAllGoods().size(), size);
        utility.deleteAll();
    }

    @Test
    public void deleteGoodByNameValidation() {
        int size = controller.getAllGoods().size();
        Good good = utility.getGood();

        controller.addOrUpdateGood(good);
        assertEquals(controller.getAllGoods().size(), size + 1);
        good.setId(controller.getAllGoods().get(size).getId());

        Good goodToDelete = new Good();
        goodToDelete.setName(good.getName());
        controller.deleteGoodByName(goodToDelete);
        assertEquals(controller.getAllGoods().size(), size);
        utility.deleteAll();
    }
}