package course.controller.rest;

import course.entity.Good;
import course.entity.Rack;
import course.entity.Sale;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class SalesRestControllerTests {

    @Autowired
    SalesRestController saleController;
    @Autowired
    WarehouseRestController warehouseController;

    @Autowired
    GoodServiceUtility utility;

    @Test
    public void addAndDeleteAllValidation() {
        int size = saleController.getAllSales().size();
        Good good = utility.getGood();

        Sale sale1 = new Sale(good.getId(), 1);
        Sale sale2 = new Sale(good.getId(), 1);

        saleController.addOrUpdateSale(sale1);
        saleController.addOrUpdateSale(sale2);
        assertEquals(saleController.getAllSales().size(), size + 2);

        saleController.deleteAllSales();
        assertEquals(saleController.getAllSales().size(), 0);

        utility.deleteAll();
    }

    @Test
    public void deleteGoodByIdValidation() {
        int size = saleController.getAllSales().size();
        Good good = utility.getGood();

        Sale sale = new Sale(good.getId(), 1);

        saleController.addOrUpdateSale(sale);
        assertEquals(saleController.getAllSales().size(), size + 1);
        good.setId(saleController.getAllSales().get(size).getId());

        Sale saleToDelete = new Sale();
        saleToDelete.setId(good.getId());
        saleController.deleteSaleById(saleToDelete);
        assertEquals(saleController.getAllSales().size(), size);

        utility.deleteAll();
    }

    @Test
    public void saleMethodValidation() {
        Good good = utility.getGood();
        long warehouseNumber = 1;
        long goods = 2;
        long goodsToSale = 1;
        Rack rack1 = new Rack(good.getId(), goods, warehouseNumber);
        warehouseController.addOrUpdateRack(rack1);

        Sale sale = new Sale(good.getId(), goodsToSale);
        sale.setWarehouse_number(warehouseNumber);
        saleController.sale(sale);

        List<Rack> result = warehouseController.getAllFromSelectedWarehouse(warehouseNumber);
        assertEquals(goods - goodsToSale, result.get(result.size() - 1).getGood_count());

        saleController.deleteAllSales();
        warehouseController.deleteAllRacks();
        utility.deleteAll();
    }
}