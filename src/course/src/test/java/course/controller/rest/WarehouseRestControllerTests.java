package course.controller.rest;

import course.entity.Good;
import course.entity.Rack;
import course.entity.Sale;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class WarehouseRestControllerTests {

    @Autowired
    WarehouseRestController controller;

    @Autowired
    GoodServiceUtility utility;

    int warehouseMaxNumber = 2;

    @Test
    public void addAndDeleteAllValidation() {
        int size = controller.getAllRacks().size();
        Good good = utility.getGood();

        Rack rack1 = new Rack(good.getId(), 1, warehouseMaxNumber);
        Rack rack2 = new Rack(good.getId(), 1, warehouseMaxNumber);

        controller.addOrUpdateRack(rack1);
        controller.addOrUpdateRack(rack2);
        assertEquals(controller.getAllRacks().size(), size + 1);

        controller.deleteAllRacks();
        assertEquals(controller.getAllRacks().size(), 0);

        utility.deleteAll();
    }

    @Test
    public void deleteGoodByIdValidation() {
        int size = controller.getAllRacks().size();
        Good good = utility.getGood();

        Rack rack = new Rack(good.getId(), 1, warehouseMaxNumber);

        controller.addOrUpdateRack(rack);
        assertEquals(controller.getAllRacks().size(), size + 1);
        good.setId(controller.getAllRacks().get(size).getId());

        Rack rackToDelete = new Rack();
        rackToDelete.setId(good.getId());
        controller.deleteRackById(rackToDelete);
        assertEquals(controller.getAllRacks().size(), size);

        utility.deleteAll();
    }

    @Test
    public void setAndGetWarehouseValidation() {
        Rack rack = new Rack();
        rack.setWarehouse_number((long) warehouseMaxNumber);
        controller.setWarehouse(rack);
        assertEquals(controller.getWarehouse().getWarehouseNumber(), warehouseMaxNumber);
    }

    @Test
    public void setGetAllValidation() {
        long[] originalSize = new long[warehouseMaxNumber];
        for (int index = 1; index < warehouseMaxNumber; index++) {
            Rack tmp = new Rack();
            tmp.setWarehouse_number((long) index);
            controller.setWarehouse(tmp);
            assertEquals(controller.getAllFromSelectedWarehouse(index).size(), controller.getAllFromSelectedWarehouse().size());
            originalSize[index] = controller.getAllFromSelectedWarehouse().size();
        }

        Rack rack = new Rack();
        rack.setWarehouse_number((long) warehouseMaxNumber);
        controller.setWarehouse(rack);

        int size = controller.getAllRacks().size();
        Good good = utility.getGood();

        rack = new Rack(good.getId(), 1, warehouseMaxNumber);

        controller.addOrUpdateRack(rack);
        assertEquals(controller.getAllRacks().size(), size + 1);

        for (int index = 1; index < warehouseMaxNumber; index++) {
            Rack tmp = new Rack();
            tmp.setWarehouse_number((long) index);
            controller.setWarehouse(tmp);
            assertEquals(controller.getAllFromSelectedWarehouse().size(), originalSize[index]);
            assertEquals(controller.getAllFromSelectedWarehouse(index).size(), originalSize[index]);
        }

        controller.deleteAllRacks();
        utility.deleteAll();

    }

    @Test
    public void addKnownnumMethodsValidation() {
        Rack init = new Rack();
        init.setWarehouse_number((long) warehouseMaxNumber);
        controller.setWarehouse(init);

        int size = controller.getAllRacks().size();
        Good good = utility.getGood();

        Rack rack1 = new Rack(good.getId(), 1, warehouseMaxNumber);

        controller.addRackKnownNum(rack1);
        assertEquals(controller.getAllRacks().size(), size + 1);

        Sale sale = new Sale(rack1.getGood_id(), rack1.getGood_count());
        sale.setWarehouse_number((long) warehouseMaxNumber);
        controller.addToCountByIDAndWarehouseNumber(sale);
        assertEquals(controller.getAllRacks().get(size).getGood_count(), rack1.getGood_count() * 2);

        controller.deleteAllRacks();
        assertEquals(controller.getAllRacks().size(), 0);

        utility.deleteAll();

    }

}