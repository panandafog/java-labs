package course;

import course.controller.rest.GoodsRestController;
import course.controller.rest.SalesRestController;
import course.controller.rest.TruncatingRestController;
import course.controller.rest.WarehouseRestController;
import course.entity.Good;
import course.entity.Rack;
import course.entity.Sale;
import course.utility.GoodServiceUtility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TruncateTests {

    @Autowired
    TruncatingRestController truncateController;
    @Autowired
    GoodsRestController goodController;
    @Autowired
    SalesRestController saleController;
    @Autowired
    WarehouseRestController warehouseController;

    @Autowired
    GoodServiceUtility utility;

    @Test
    public void truncateAllValidation() {
        Good good = utility.getGood();
        goodController.addOrUpdateGood(good);
        good = utility.getGood();
        goodController.addOrUpdateGood(good);

        good = utility.getGood();

        Sale sale1 = new Sale(good.getId(), 1);
        Sale sale2 = new Sale(good.getId(), 1);

        saleController.addOrUpdateSale(sale1);
        saleController.addOrUpdateSale(sale2);

        good = utility.getGood();

        Rack rack1 = new Rack(good.getId(), 1, 1);
        Rack rack2 = new Rack(good.getId(), 1, 1);

        warehouseController.addOrUpdateRack(rack1);
        warehouseController.addOrUpdateRack(rack2);

        truncateController.truncateAllTables();

        assertEquals(0, goodController.getAllGoods().size());
        assertEquals(0, saleController.getAllSales().size());
        assertEquals(0, warehouseController.getAllFromSelectedWarehouse(1).size());
    }

    @Test
    public void truncateSalesValidation() {
        Good good = utility.getGood();

        Sale sale1 = new Sale(good.getId(), 1);
        Sale sale2 = new Sale(good.getId(), 1);

        saleController.addOrUpdateSale(sale1);
        saleController.addOrUpdateSale(sale2);

        truncateController.truncateSales();

        assertEquals(0, saleController.getAllSales().size());
    }

    @Test
    public void truncateWarehousesValidation() {
        Good good = utility.getGood();

        Rack rack1 = new Rack(good.getId(), 1, 1);
        Rack rack2 = new Rack(good.getId(), 1, 1);

        warehouseController.addOrUpdateRack(rack1);
        warehouseController.addOrUpdateRack(rack2);

        truncateController.truncateAllTables();

        assertEquals(0, warehouseController.getAllFromSelectedWarehouse(1).size());
    }
}
