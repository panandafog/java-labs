package course.entity;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaleTests {
    @Test
    public void basicValidation() {

        long goodID = 1;
        long goodCount = 1;
        Date date = new Date();

        Sale sale = new Sale(goodID, goodCount);

        assertEquals(sale.getGood_id(), goodID);
        assertEquals(sale.getGood_count(), goodCount);
        assertEquals(sale.getCreate_date().toString(), date.toString());

        sale = new Sale(goodID, goodCount, date);

        assertEquals(sale.getGood_id(), goodID);
        assertEquals(sale.getGood_count(), goodCount);
        assertEquals(sale.getCreate_date(), date);

        long warehouseNumber = 2;
        long id = 1;
        Good good = new Good();
        goodCount = 2;
        date.setTime(date.getTime() + 1);

        sale.setId(id);
        sale.setGood(good);
        sale.setGood_count(goodCount);
        sale.setCreate_date(date);
        sale.setWarehouse_number(warehouseNumber);

        assertEquals(sale.getId(), id);
        assertEquals(sale.getGood(), good);
        assertEquals(sale.getGood_count(), goodCount);
        assertEquals(sale.getCreate_date(), date);
        assertEquals(sale.getWarehouse_number(), warehouseNumber);

    }
}
