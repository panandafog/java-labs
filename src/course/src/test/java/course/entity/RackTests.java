package course.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RackTests {
    @Test
    public void basicValidation() {

        long goodID = 1;
        long goodCount = 1;
        long warehouseNumber = 1;

        Rack rack = new Rack(goodID, goodCount, warehouseNumber);

        assertEquals(rack.getGood_id(), goodID);
        assertEquals(rack.getGood_count(), goodCount);
        assertEquals(rack.getWarehouse_number(), warehouseNumber);

        long id = 1;
        goodCount = 2;
        warehouseNumber = 2;
        Good good = new Good();

        rack.setId(id);
        rack.setGood(good);
        rack.setGood_count(goodCount);
        rack.setWarehouse_number(warehouseNumber);

        assertEquals(rack.getId(), id);
        assertEquals(rack.getGood(), good);
        assertEquals(rack.getGood_count(), goodCount);
        assertEquals(rack.getWarehouse_number(), warehouseNumber);

    }
}
