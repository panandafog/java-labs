package course.entity;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoleTests {
    @Test
    public void basicValidation() {

        long ID = 1;
        String name = "name";

        Role role = new Role(ID);

        assertEquals(role.getId(), ID);

        role = new Role(ID, name);

        assertEquals(role.getId(), ID);
        assertEquals(role.getName(), name);

        ID = 2;
        name = "new name";
        Set<User> users = new HashSet<>(Arrays.asList(new User[]{}));

        role.setId(ID);
        role.setName(name);
        role.setUsers(users);

        assertEquals(role.getId(), ID);
        assertEquals(role.getName(), name);
        assertEquals(role.getUsers(), users);

    }
}
