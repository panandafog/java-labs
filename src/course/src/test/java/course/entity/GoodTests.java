package course.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GoodTests {
    @Test
    public void basicValidation() {

        String name = "name";
        float priority = (float) 1.0;

        Good good = new Good(name, priority);

        assertEquals(good.getName(), name);
        assertEquals(good.getPriority(), priority);

        long ID = 2;
        name = "new name";
        priority += 1;

        good.setId(ID);
        good.setName(name);
        good.setPriority(priority);

        assertEquals(good.getId(), ID);
        assertEquals(good.getName(), name);
        assertEquals(good.getPriority(), priority);

    }
}
