package course.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTests {
    @Test
    public void basicValidation() {

        String username = "username";
        String password = "password";
        String passwordConfirm = "password";

        User user = new User(username, password, passwordConfirm);

        assertEquals(user.getUsername(), username);
        assertEquals(user.getPassword(), password);
        assertEquals(user.getPasswordConfirm(), passwordConfirm);

        username = "new username";
        password = "new password";
        passwordConfirm = "new password";

        user.setUsername(username);
        user.setPassword(password);
        user.setPasswordConfirm(passwordConfirm);

        assertEquals(user.getUsername(), username);
        assertEquals(user.getPassword(), password);
        assertEquals(user.getPasswordConfirm(), passwordConfirm);
    }

}
